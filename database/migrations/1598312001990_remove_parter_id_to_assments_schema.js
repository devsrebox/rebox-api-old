'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RemoveParterIdToAssmentsSchema extends Schema {
  up () {
    this.table('assessments', (table) => {
      table.dropForeign('parter_id')
      table.dropColumn('parter_id')
      table.integer('rebox_service_id')
      .unsigned()
      .references('id')
      .inTable('rebox_services')
      .after('id')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')

    })
  }

  down () {
    this.table('assessments', (table) => {
      // reverse alternations
    })
  }
}

module.exports = RemoveParterIdToAssmentsSchema
