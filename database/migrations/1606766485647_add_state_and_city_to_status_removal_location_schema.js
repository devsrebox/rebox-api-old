'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddStateAndCityToStatusRemovalLocationSchema extends Schema {
  up () {
    this.table('removal_locations', (table) => {
      table.string('state', 2)
      table.string('city')
    })
    this.table('status_vehicles', (table) => {
      table.string('state', 2)
      table.string('city')
    })
  }

  down () {
    this.table('add_state_and_city_to_status_removal_locations', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddStateAndCityToStatusRemovalLocationSchema
