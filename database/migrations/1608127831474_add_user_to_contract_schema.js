'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddUserToContractSchema extends Schema {
  up () {
    this.table('contracts', (table) => {
      table
      .integer("user_id")
      .unsigned()
      .references("id")
      .inTable("users")
      .onUpdate("CASCADE")
      .onDelete("CASCADE");
    })
  }

  down () {
    this.table('add_user_to_contracts', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddUserToContractSchema
