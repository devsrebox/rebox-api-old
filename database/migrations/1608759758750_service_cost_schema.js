'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ServiceCostSchema extends Schema {
  up () {
    this.create('service_costs', (table) => {
      table.increments()
      table.string('description')
      table.float('cost')
      table.string('state', 2)
      table.string('city', 400)
      table.timestamps()
    })
  }

  down () {
    this.drop('service_costs')
  }
}

module.exports = ServiceCostSchema
