'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TypesServicesSchema extends Schema {
  up () {
    this.create('types_services', (table) => {
      table.increments()
      table.string('description')
      table.timestamps()
    })

  }

  down () {
    this.drop('types_services')
  }
}

module.exports = TypesServicesSchema
