"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddLngLatToConsumedPlansSchema extends Schema {
  up() {
    this.table("consumed_plans", (table) => {
      table.string("lng");
      table.string("lat");
    });
  }

  down() {
    this.table("add_lng_lat_to_consumed_plans", (table) => {
      // reverse alternations
    });
  }
}

module.exports = AddLngLatToConsumedPlansSchema;
