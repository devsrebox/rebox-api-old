'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddImgProfileToUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('img_profile').after('cpf')
    })
  }

  down () {
    this.table('add_img_profile_to_users', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddImgProfileToUserSchema
