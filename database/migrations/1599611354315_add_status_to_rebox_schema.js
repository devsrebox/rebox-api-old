'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddStatusToReboxSchema extends Schema {
  up () {
    this.table('rebox_services', (table) => {
      table.enu('status', ['pendente', 'aceito', 'andamento', 'finalizado']).defaultTo('pendente')
    })
  }

  down () {
    this.table('add_status_to_reboxes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddStatusToReboxSchema
