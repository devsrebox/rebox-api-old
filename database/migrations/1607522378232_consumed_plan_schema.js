'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ConsumedPlanSchema extends Schema {
  up () {
    this.create('consumed_plans', (table) => {
      table.increments()
      table
      .integer('users_plan_id')
      .unsigned()
      .references('id')
      .inTable('users_plans')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.boolean('is_approved').defaultTo(false)
      table.datetime('aproved_date')
      table.string('user_aproved')
      table.string('status').defaultTo('pendente')
      table.timestamps()
    })
  }

  down () {
    this.drop('consumed_plans')
  }
}

module.exports = ConsumedPlanSchema
