'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddFlagPlanToReboxSchema extends Schema {
  up () {
    this.table('rebox_prices', (table) => {
      table.boolean('is_plan').defaultTo(false)

    })
  }

  down () {
    this.table('add_flag_plan_to_reboxes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddFlagPlanToReboxSchema
