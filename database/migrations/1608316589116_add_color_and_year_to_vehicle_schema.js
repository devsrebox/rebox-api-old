'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddColorAndYearToVehicleSchema extends Schema {
  up () {
    this.table('vehicles', (table) => {
      table.string('color')
    })
  }

  down () {
    this.table('add_color_and_year_to_vehicles', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddColorAndYearToVehicleSchema
