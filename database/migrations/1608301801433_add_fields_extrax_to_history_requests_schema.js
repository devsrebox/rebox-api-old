'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddFieldsExtraxToHistoryRequestsSchema extends Schema {
  up () {
    this.table('history_requests', (table) => {
      table.string('description')
      table.float('price')
      table.string('type')
    })
  }

  down () {
    this.table('add_fields_extrax_to_history_requests', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddFieldsExtraxToHistoryRequestsSchema
