'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddStateToPricesSchema extends Schema {
  up () {
    this.table('rebox_prices', (table) => {
      table.string('state', 2)
    })
    this.table('rebox_services', (table) => {
      table.string('state', 2)
    })
  }

  down () {
    this.table('add_state_to_prices', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddStateToPricesSchema
