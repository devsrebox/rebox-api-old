'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddAdressToUsersAndDetailsToVehicleSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('state')
      table.string('city')
    })
    this.table('vehicles', (table) => {
      table.string('motor')
      table.integer('year')
      table.integer('oil_changes')
    })
  }

  down () {
    this.table('add_adress_to_users_and_details_to_vehicles', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddAdressToUsersAndDetailsToVehicleSchema
