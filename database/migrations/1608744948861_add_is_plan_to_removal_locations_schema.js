'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddIsPlanToRemovalLocationsSchema extends Schema {
  up () {
    this.table('removal_locations', (table) => {
      table.boolean('is_plan').defaultTo(false)
    })
  }

  down () {
    this.table('add_is_plan_to_removal_locations', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddIsPlanToRemovalLocationsSchema
