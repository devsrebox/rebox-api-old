'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StatusPaymentsSchema extends Schema {
  up () {
    this.create('status_payments', (table) => {
      table.increments()
      table.integer('rebox_service_id')
      .unsigned()
      .references('id')
      .inTable('rebox_services')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.string('description')
      table.enu('payment_method', ['boleto', 'cartao', 'debito'])
      table.timestamps()
    })
  }

  down () {
    this.drop('status_payments')
  }
}

module.exports = StatusPaymentsSchema
