'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddLatLngToConsumedPlansSchema extends Schema {
  up () {
    this.table('consumed_plans', (table) => {
      table.renameColumn('lng', 'lng1')
      table.renameColumn('lat', 'lat1')

      table.string('lng2')
      table.string('lat2')
    })
  }

  down () {
    this.table('add_lat_lng_to_consumed_plans', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddLatLngToConsumedPlansSchema
