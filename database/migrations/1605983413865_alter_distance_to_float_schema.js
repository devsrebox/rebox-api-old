'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterDistanceToFloatSchema extends Schema {
  up () {
    this.table('rebox_services', (table) => {
      table.float('distance').alter()
    })
  }

  down () {
    this.table('alter_distance_to_floats', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterDistanceToFloatSchema
