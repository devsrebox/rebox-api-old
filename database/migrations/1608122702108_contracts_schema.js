'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContractsSchema extends Schema {
  up () {
    this.create('contracts', (table) => {
      table.increments()
      table
      .integer("plan_id")
      .unsigned()
      .references("id")
      .inTable("plans")
      .onUpdate("CASCADE")
      .onDelete("CASCADE");
      table.integer('uses')
      table.date('validity_of')
      table.date('validity_until')
      table.timestamps()
    })

    this.table("users_plans", (table) => {
      table
      .integer("contract_id")
      .unsigned()
      .references("id")
      .inTable("contracts")
      .onUpdate("CASCADE")
      .onDelete("CASCADE");
    })
  }

  down () {
    this.drop('contracts')
  }
}

module.exports = ContractsSchema
