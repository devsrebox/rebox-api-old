'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReboxPriceSchema extends Schema {
  up () {
    this.create('rebox_prices', (table) => {
      table.increments()
      table.float('price').defaultTo(0)
      table.integer('km').defaultTo(0)
      table.float('per_km').defaultTo(0)
      table.float('commission').defaultTo(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('rebox_prices')
  }
}

module.exports = ReboxPriceSchema
