'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StatusVehiclesSchema extends Schema {
  up () {
    this.create('status_vehicles', (table) => {
      table.increments()
      table.string('description')
      table.timestamps()
    })
  }

  down () {
    this.drop('status_vehicles')
  }
}

module.exports = StatusVehiclesSchema
