'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PortCarsSchema extends Schema {
  up () {
    this.create('port_cars', (table) => {
      table.increments()
      table.string('port')
      table.float('percent_value')
      table.float('percent_extra')
      table.timestamps()
    })
  }

  down () {
    this.drop('port_cars')
  }
}

module.exports = PortCarsSchema
