'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddValueToRemoveLocationsSchema extends Schema {
  up () {
    this.table('removal_locations', (table) => {
      table.float('price').defaultTo(0)
    })
  }

  down () {
    this.table('removal_locations', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddValueToRemoveLocationsSchema
