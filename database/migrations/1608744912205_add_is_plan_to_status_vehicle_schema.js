'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddIsPlanToStatusVehicleSchema extends Schema {
  up () {
    this.table('status_vehicles', (table) => {
      table.boolean('is_plan').defaultTo(false)
    })
  }

  down () {
    this.table('add_is_plan_to_status_vehicles', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddIsPlanToStatusVehicleSchema
