'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PartinersTypesSchema extends Schema {
  up () {
    this.create('partners_types', (table) => {
      table.increments()
      table.integer('types_service_id')
      .unsigned()
      .references('id')
      .inTable('types_services')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.integer('partner_id')
      .unsigned()
      .references('id')
      .inTable('partners')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('partners_types')
  }
}

module.exports = PartinersTypesSchema
