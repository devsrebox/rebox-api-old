'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddVehicleToContractsSchema extends Schema {
  up () {
    this.table('contracts', (table) => {
      table
      .integer("vehicle_id")
      .unsigned()
      .references("id")
      .inTable("vehicles")
      .onUpdate("CASCADE")
      .onDelete("CASCADE");
    })
  }

  down () {
    this.table('add_vehicle_to_contracts', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddVehicleToContractsSchema
