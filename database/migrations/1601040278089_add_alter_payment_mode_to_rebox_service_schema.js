'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddAlterPaymentModeToReboxServiceSchema extends Schema {
  up () {
    this.table('status_payments', (table) => {
      table.dropColumn('payment_method')
    })
    this.table('status_payments', (table) => {
      table.enu('payment_method', ['dinheiro', 'credito', 'debito'])
    })
  }
  down () {
    this.table('status_payments', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddAlterPaymentModeToReboxServiceSchema
