'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RemoveStatusSchema extends Schema {
  up () {
    this.table('rebox_services', (table) => {
      table.dropColumn('status')

    })
    this.table('vehicles', (table) => {
      table.dropForeign('status_vehicle_id')
      table.dropColumn('status_vehicle_id')

    })
  }
}

module.exports = RemoveStatusSchema
