'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterKmToFloatSchema extends Schema {
  up () {
    this.table('rebox_prices', (table) => {
      table.float('km').alter()
    })
  }

  down () {
    this.table('alter_km_to_floats', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterKmToFloatSchema
