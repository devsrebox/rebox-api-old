'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.increments()
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.string('cpf', 50)
      table.string('name', 255)
      table.enu('role', ['user', 'partner', 'admin']).defaultTo('user')
      table.decimal('lat', 10, 8)
      table.decimal('lng', 11, 8)
      table.string('cel')
      table.string('tel')
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
