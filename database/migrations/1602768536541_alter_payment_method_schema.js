'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')
const Database = use('Database')
const Env = use('Env')
const formatAlterTableEnumSql = (
  tableName,
  columnName,
  enums,
) => {
  const constraintName = `${tableName}_${columnName}_check`;
  return [
    `ALTER TABLE ${tableName} DROP CONSTRAINT IF EXISTS ${constraintName};`,
    `ALTER TABLE ${tableName} ADD CONSTRAINT ${constraintName} CHECK (${columnName} = ANY (ARRAY['${enums.join(
      "'::text, '"
    )}'::text]));`,
  ].join('\n');
};

class AlterPaymentMethodSchema extends Schema {
  async up() {
    if (Env.get('DB_CONNECTION') == 'pg') {
      await Database.raw(
        formatAlterTableEnumSql('status_payments', 'payment_method', [
          'dinheiro', 'credito', 'debito', 'presencial'
        ])
      );

    } else {
      this.table('status_payments', (table) => {
        table.enu('payment_method', [
          'dinheiro', 'credito', 'debito', 'presencial'
        ]).alter()
      })
    }


  }

  down() {
    this.table('status_payments', (table) => {
      // reverse alternations
    })
  }


}

module.exports = AlterPaymentMethodSchema
