'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AttendantsSchema extends Schema {
  up () {
    this.create('attendants', (table) => {
      table.increments()
      table.integer('user_id')
      .unsigned()
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.integer('partner_id')
      .unsigned()
      .references('id')
      .inTable('partners')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('attendants')
  }
}

module.exports = AttendantsSchema
