'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PaymentsPlansSchema extends Schema {
  up () {
    this.create('payments_plans', (table) => {
      table.increments()
      table
      .integer('user_id')
      .unsigned()
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table
      .integer('plan_id')
      .unsigned()
      .references('id')
      .inTable('plans')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.string('method_payment')
      table.string('status')
      table.string('status_details')
      table.string('status_code')
      table.timestamps()
    })
  }

  down () {
    this.drop('payments_plans')
  }
}

module.exports = PaymentsPlansSchema
