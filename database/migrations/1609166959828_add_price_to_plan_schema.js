'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddPriceToPlanSchema extends Schema {
  up () {
    this.table('contracts', (table) => {
      table.float('price')
      table.string('status').defaultTo('sold')
    })
  }

  down () {
    this.table('contracts', (table) => {
      table.dropColumn('price')
    })
  }
}

module.exports = AddPriceToPlanSchema
