'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddUsersPlansToReboxServiceSchema extends Schema {
  up () {
    this.table('rebox_services', (table) => {
      table
      .integer("users_plan_id")
      .unsigned()
      .references("id")
      .inTable("users_plans")
    })
  }

  down () {
    this.table('add_users_plans_to_rebox_services', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddUsersPlansToReboxServiceSchema
