'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddValueToStatusVehiclesSchema extends Schema {
  up () {
    this.table('status_vehicles', (table) => {
      table.float('price').defaultTo(0)
    })
  }

  down () {
    this.table('add_value_to_status_vehicles', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddValueToStatusVehiclesSchema
