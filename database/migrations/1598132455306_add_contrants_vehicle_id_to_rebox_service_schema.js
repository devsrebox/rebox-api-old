'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddContrantsVehicleIdToReboxServiceSchema extends Schema {
  up () {
    this.table('rebox_services', (table) => {
      table.integer('vehicle_id')
      .unsigned()
      .references('id')
      .inTable('vehicles')
      .after('id')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.integer('status_vehicle_id')
      .unsigned()
      .references('id')
      .inTable('status_vehicles')
      .after('id')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')

    })
  }

  down () {
    this.table('add_contrants_vehicle_id_to_rebox_services', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddContrantsVehicleIdToReboxServiceSchema
