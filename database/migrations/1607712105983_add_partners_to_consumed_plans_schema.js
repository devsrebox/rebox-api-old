"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");
class AddPartnersToConsumedPlansSchema extends Schema {
  up() {
    this.table("consumed_plans", (table) => {
      table
        .integer("partner_id")
        .unsigned()
        .references("id")
        .inTable("partners")
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
    });
  }

  down() {
    this.table("add_partners_to_consumed_plans", (table) => {
      // reverse alternations
    });
  }
}

module.exports = AddPartnersToConsumedPlansSchema;
