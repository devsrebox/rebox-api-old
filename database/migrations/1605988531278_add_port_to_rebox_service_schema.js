'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')
const ReboxService = use('App/Models/ReboxService')
const Car = use('App/Models/Car')
class AddPortToReboxServiceSchema extends Schema {
  async up() {
    this.table('rebox_services', async (table) => {
      table.integer('port_car_id')
        .unsigned()
        .references('id')
        .inTable('port_cars')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')

    })


  }

  down() {
    this.table('add_port_to_rebox_services', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddPortToReboxServiceSchema
