'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddStatusToPartnerSchema extends Schema {
  up () {
    this.table('partners', (table) => {
      table.boolean('status').defaultTo(false)
    })
  }

  down () {
    this.table('add_status_to_partners', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddStatusToPartnerSchema
