'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PartnersSchema extends Schema {
  up () {
    this.create('partners', (table) => {
      table.increments()
      table
      .integer('user_id')
      .unsigned()
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.string('contact1')
      table.string('contact2')
      table.integer('number_trucks')
      table.integer('cod')
      table.string('cnpj')
      table.string('company_name')
      table.string('name')
      table.string('ie')
      table.string('im')
      table.string('cep')
      table.string('address')
      table.string('city')
      table.string('uf')
      table.string('neighborhood')
      table.date('contract_of')
      table.date('contract_until')


      table.timestamps()
    })
  }

  down () {
    this.drop('partners')
  }
}

module.exports = PartnersSchema
