'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReboxExtractSchema extends Schema {
  up () {
    this.create('rebox_extracts', (table) => {
      table.increments()
      table.integer('rebox_service_id')
      .unsigned()
      .references('id')
      .inTable('rebox_services')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.string('description')
      table.float('value').defaultTo(0)
      table.float('commission_percent').defaultTo(0)
      table.float('rebox_value').defaultTo(0)
      table.float('partner_value').defaultTo(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('rebox_extracts')
  }
}

module.exports = ReboxExtractSchema
