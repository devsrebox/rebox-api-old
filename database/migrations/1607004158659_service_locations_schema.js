'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ServiceLocationsSchema extends Schema {
  up () {
    this.create('serviceLocations', (table) => {
      table.increments()
      table.integer('partner_id')
      .unsigned()
      .references('id')
      .inTable('partners')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.string('state')
      table.string('city')
      table.timestamps()
    })
  }

  down () {
    this.drop('serviceLocations')
  }
}

module.exports = ServiceLocationsSchema
