'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RemovalLocationsSchema extends Schema {
  up () {
    this.create('removal_locations', (table) => {
      table.increments()
      table.string('description')
      table.timestamps()
    })
  }

  down () {
    this.drop('removal_locations')
  }
}

module.exports = RemovalLocationsSchema
