'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddNamePartnerToReboxSchema extends Schema {
  up () {
    this.table('rebox_services', (table) => {
      table.string('name_partner')
    })
  }

  down () {
    this.table('add_name_partner_to_reboxes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddNamePartnerToReboxSchema
