'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddComplementarFieldsToUsersPlansSchema extends Schema {
  up () {
    this.table('users_plans', (table) => {
      table.float('km_max')
      table.integer('uses')
      table.float('cover_value')
      table.float('price_pg')
      table.boolean('is_active')
    })
  }

  down () {
    this.table('add_complementar_fields_to_users_plans', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddComplementarFieldsToUsersPlansSchema
