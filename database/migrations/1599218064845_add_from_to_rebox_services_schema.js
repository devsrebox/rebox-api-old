'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddFromToReboxServicesSchema extends Schema {
  up () {
    this.table('rebox_services', (table) => {
      table.string('from', 500)
      table.string('to', 500)
      table.string('distance', 10)
    })
  }

  down () {
    this.table('rebox_services', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddFromToReboxServicesSchema
