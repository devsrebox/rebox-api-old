'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddAttendantToReboxSchema extends Schema {
  up () {
    this.table('rebox_services', (table) => {
      table.integer('attendant_id')
      .unsigned()
      .references('id')
      .inTable('attendants')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
    })
  }

  down () {
    this.table('add_attendant_to_reboxes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddAttendantToReboxSchema
