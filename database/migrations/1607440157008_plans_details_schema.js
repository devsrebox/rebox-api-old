'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlansDetailsSchema extends Schema {
  up () {
    this.create('plans_details', (table) => {
      table.increments()
      table.integer('plan_id')
      .unsigned()
      .references('id')
      .inTable('plans')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.string('description')
      table.integer('number_uses')
      table.float('km_max')
      table.float('cover_value')
      table.timestamps()
    })
  }

  down () {
    this.drop('plans_details')
  }
}

module.exports = PlansDetailsSchema
