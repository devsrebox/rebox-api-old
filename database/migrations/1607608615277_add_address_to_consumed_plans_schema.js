'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddAddressToConsumedPlansSchema extends Schema {
  up () {
    this.table('consumed_plans', (table) => {
      table.string('address', 600)
    })
  }

  down () {
    this.table('add_address_to_consumed_plans', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddAddressToConsumedPlansSchema
