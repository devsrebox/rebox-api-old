'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddCompositeKeyToStatusRemovalSchema extends Schema {
  up () {
    this.table('removal_locations', (table) => {
      table.integer('location_id')
    })
    this.table('status_vehicles', (table) => {
      table.integer('location_id')
    })
  }

  down () {
    this.table('add_composite_key_to_status_removals', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddCompositeKeyToStatusRemovalSchema
