'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddContrantToCarsSchema extends Schema {
  up () {
    this.table('cars', (table) => {
      table.integer('port_car_id')
      .unsigned()
      .references('id')
      .inTable('port_cars')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      .after('id')
    })
  }

  down () {
    this.table('add_contrant_to_cars', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddContrantToCarsSchema
