'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersPlansSchema extends Schema {
  up () {
    this.create('users_plans', (table) => {
      table.increments()
      table
      .integer('user_id')
      .unsigned()
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table
      .integer('plans_detail_id')
      .unsigned()
      .references('id')
      .inTable('plans_details')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.string('description')
      table.date('validity_of')
      table.date('validity_until')
      table.string('method_payment')

      table.timestamps()
    })
  }

  down () {
    this.drop('users_plans')
  }
}

module.exports = UsersPlansSchema
