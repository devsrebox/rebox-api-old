'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddTypePaymentModeSchema extends Schema {
  up () {
    this.table('status_payments', (table) => {
      table.string('type_payment_mode')
    })
  }

  down () {
    this.table('status_payments', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddTypePaymentModeSchema
