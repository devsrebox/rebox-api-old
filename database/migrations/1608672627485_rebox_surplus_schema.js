'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReboxSurplusSchema extends Schema {
  up () {
    this.create('rebox_surpluses', (table) => {
      table.increments()
      table
      .integer("port_car_id")
      .unsigned()
      .references("id")
      .inTable("port_cars")
      .onUpdate("CASCADE")
      .onDelete("CASCADE");
      table.string('state', 400)
      table.string('city', 400)
      table.float('rebox_commission')
      table.timestamps()
    })
  }

  down () {
    this.drop('rebox_surpluses')
  }
}

module.exports = ReboxSurplusSchema
