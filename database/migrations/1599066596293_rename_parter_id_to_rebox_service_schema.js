'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RenameParterIdToReboxServiceSchema extends Schema {
  up () {
    this.table('rebox_services', (table) => {
      table.dropForeign('parter_id')
      table.dropColumn('parter_id')
      table.integer('partner_id')
      .unsigned()
      .references('id')
      .inTable('partners')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      .after('id')
    })
  }
}

module.exports = RenameParterIdToReboxServiceSchema
