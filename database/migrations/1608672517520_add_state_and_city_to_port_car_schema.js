'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddStateAndCityToPortCarSchema extends Schema {
  up () {
    this.table('port_cars', (table) => {
      table.string('state', 400)
      table.string('city', 400)
    })
  }

  down () {
    this.table('add_state_and_city_to_port_cars', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddStateAndCityToPortCarSchema
