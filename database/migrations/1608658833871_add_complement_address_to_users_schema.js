'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddComplementAddressToUsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('address_zip_code')
      table.string('address_neighborhood')
      table.string('address_street')
    })
  }

  down () {
    this.table('add_complement_address_to_users', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddComplementAddressToUsersSchema
