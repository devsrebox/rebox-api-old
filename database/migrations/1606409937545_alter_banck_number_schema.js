'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterBanckNumberSchema extends Schema {
  up () {
    this.table('bank_accounts', (table) => {
      table.renameColumn('bank_name', 'bank_number')
    })
  }

  down () {
    this.table('alter_banck_numbers', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterBanckNumberSchema
