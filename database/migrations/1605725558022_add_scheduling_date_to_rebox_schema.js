'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')
const knex = use('Database')
class AddSchedulingDateToReboxSchema extends Schema {
  up () {
    this.table('rebox_services', (table) => {
      table.datetime('scheduling_date', { precision: 6 }).defaultTo(knex.fn.now(6))
    })
  }

  down () {
    this.table('add_scheduling_date_to_reboxes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddSchedulingDateToReboxSchema
