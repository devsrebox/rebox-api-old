'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddAmountToReboxServiceSchema extends Schema {
  up () {
    this.table('rebox_services', (table) => {
      table.float('amount').after('id')
    })
  }

  down () {
    this.table('rebox_services', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddAmountToReboxServiceSchema
