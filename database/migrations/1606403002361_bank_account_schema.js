'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BankAccountSchema extends Schema {
  up () {
    this.create('bank_accounts', (table) => {
      table.increments()
      table.integer('rebox_service_id')
      .unsigned()
      .references('id')
      .inTable('rebox_services')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.integer('partner_id')
      table.string('bank_name')
      table.string('account_agency')
      table.string('account_current')
      table.timestamps()
    })
  }

  down () {
    this.drop('bank_accounts')
  }
}

module.exports = BankAccountSchema
