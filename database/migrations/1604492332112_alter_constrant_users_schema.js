'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')
const Database = use('Database')
const Env = use('Env')
const formatAlterTableEnumSql = (
  tableName,
  columnName,
  enums,
) => {
  const constraintName = `${tableName}_${columnName}_check`;
  return [
    `ALTER TABLE ${tableName} DROP CONSTRAINT IF EXISTS ${constraintName};`,
    `ALTER TABLE ${tableName} ADD CONSTRAINT ${constraintName} CHECK (${columnName} = ANY (ARRAY['${enums.join(
      "'::text, '"
    )}'::text]));`,
  ].join('\n');
};

class AlterConstrantUsersSchema extends Schema {
  async up () {
    if (Env.get('DB_CONNECTION') == 'pg') {
      await Database.raw(
        formatAlterTableEnumSql('users', 'role', [
          'user','partner','admin', 'attendant'
        ])
      );

    } else {
      this.table('users', (table) => {
        table.enu('role', [
          'user','partner','admin', 'attendant'
        ]).alter()
      })
    }

  }

  down () {
    this.table('alter_constrant_users', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterConstrantUsersSchema
