'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReboxServicesSchema extends Schema {
  up () {
    this.create('rebox_services', (table) => {
      table.increments()
      table.integer('parter_id')
      .unsigned()
      .references('id')
      .inTable('partners')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.integer('removal_location_id')
      .unsigned()
      .references('id')
      .inTable('removal_locations')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.string('status_payment')
      table.time('forecast')
      table.string('comments')
      table.decimal('lat1', 10, 8)
      table.decimal('lng1', 11, 8)
      table.decimal('lat2', 10, 8)
      table.decimal('lng2', 11, 8)
      table.boolean('is_present').defaultTo(false)
      table.enu('status', ['pendente', 'aceito', 'andamento', 'finalizado']).defaultTo('pendente')
      table.timestamps()
    })
  }

  down () {
    this.drop('rebox_services')
  }
}

module.exports = ReboxServicesSchema
