'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddContrantToReboxPriceSchema extends Schema {
  up () {
    this.table('rebox_prices', (table) => {
      table.integer('port_car_id')
      .unsigned()
      .references('id')
      .inTable('port_cars')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      .after('id')
    })
  }

  down () {
    this.table('add_contrant_to_rebox_prices', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddContrantToReboxPriceSchema
