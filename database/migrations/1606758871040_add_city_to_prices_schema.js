'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddCityToPricesSchema extends Schema {
  up () {
    this.table('rebox_prices', (table) => {
      table.string('city')
    })
    this.table('rebox_services', (table) => {
      table.string('city')
    })
  }

  down () {
    this.table('add_city_to_prices', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddCityToPricesSchema
