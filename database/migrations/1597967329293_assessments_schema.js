'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AssessmentsSchema extends Schema {
  up () {
    this.create('assessments', (table) => {
      table.increments()
      table.integer('user_id')
      .unsigned()
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.integer('parter_id')
      .unsigned()
      .references('id')
      .inTable('partners')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.integer('value').defaultTo(0)
      table.string('comments', 400)

      table.timestamps()
    })
  }

  down () {
    this.drop('assessments')
  }
}

module.exports = AssessmentsSchema
