'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VehiclesSchema extends Schema {
  up () {
    this.create('vehicles', (table) => {
      table.increments()
      table
      .integer('user_id')
      .unsigned()
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table
      .integer('status_vehicle_id')
      .unsigned()
      .references('id')
      .inTable('status_vehicles')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
      table.string('brand')
      table.string('model')
      table.string('board')
      table.boolean('is_armored').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('vehicles')
  }
}

module.exports = VehiclesSchema
