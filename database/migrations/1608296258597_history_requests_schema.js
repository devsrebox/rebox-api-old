'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HistoryRequestsSchema extends Schema {
  up () {
    this.create('history_requests', (table) => {
      table.increments()
      table
      .integer("user_id")
      .unsigned()
      .references("id")
      .inTable("users")
      .onUpdate("CASCADE")
      .onDelete("CASCADE");
      table
      .integer("partner_id")
      .unsigned()
      .references("id")
      .inTable("partners")
      .onUpdate("CASCADE")
      .onDelete("CASCADE");
      table.integer('audity_id')
      table.string('from')
      table.string('to')
      table.timestamps()
    })
  }

  down () {
    this.drop('history_requests')
  }
}

module.exports = HistoryRequestsSchema
