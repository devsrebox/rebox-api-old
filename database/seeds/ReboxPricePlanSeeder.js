'use strict'

/*
|--------------------------------------------------------------------------
| ReboxPricePlanSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const ReboxPrice = use('App/Models/ReboxPrice')
const { STATES } = require('./cities')
class ReboxPricePlanSeeder {
  async run () {
    var prices = []
    const states = STATES
    for (const state of states) {
      for (const city of state.cidades) {
        var rebox = //veiculos de passeio
        {
          'price' : 90.00,
          'km': 40,
          'per_km': 1.50,
          'commission': 40,
          'port_car_id': 1,
          state: state.sigla,
          city,
          is_plan: true
        }
        prices.push(rebox)

      }
    }
    await ReboxPrice.createMany(prices)
  }
}

module.exports = ReboxPricePlanSeeder
