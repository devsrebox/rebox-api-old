'use strict'

/*
|--------------------------------------------------------------------------
| PortCarSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const PortCar = use('App/Models/PortCar')
const { STATES } = require('./cities')
class PortCarSeeder {
  async run () {
    for (const state of STATES) {
      for (const city of state.cidades) {
        const ports = [
          {
            port: 'Carro de Passeio',
            percent_value: 40,
            percent_extra: 0,
            city,
            state: state.sigla
          },
          {
            port: 'Tricíclo',
            percent_value: 40,
            percent_extra: 0,
            city,
            state: state.sigla
          },
          {
            port: 'Quadricíclo',
            percent_value: 40,
            percent_extra: 0,
            city,
            state: state.sigla
          },
          {
            port: 'Van',
            percent_value: 28.5,
            percent_extra: 0,
            city,
            state: state.sigla
          },
          {
            port: 'Sedan Grande Porte',
            percent_value: 40,
            percent_extra: 0,
            city,
            state: state.sigla
          },
          {
            port: 'Pickup SUV',
            percent_value: 40,
            percent_extra: 0,
            city,
            state: state.sigla
          },
          {
            port: 'Utilitário Carga',
            percent_value: 40,
            percent_extra: 0,
            city,
            state: state.sigla
          },
          {
            port: 'Moto',
            percent_value: 40,
            percent_extra: 0,
            city,
            state: state.sigla
          },
        ]

        await PortCar.createMany(ports)

      }
    }

  }
}

module.exports = PortCarSeeder
