'use strict'

const { STATES } = require('./cities')

/*
|--------------------------------------------------------------------------
| ReboxPriceSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const ReboxPrice = use('App/Models/ReboxPrice')
const ReboxService = use('App/Models/ReboxService')
const Database = use('Database')
const fs = require('fs')
const Helpers = use('Helpers')
class ReboxPriceSeeder {
  async run () {
    // const reboxs = await ReboxService.query().fetch()
    // for (const rebox of reboxs.rows) {
    //   try {
    //     rebox.state = rebox.from.split('-')[2].substr(1, 2)
    //     var city = rebox.from.split('-')[1].split(',')[1].substr(1)
    //     city = city.substr(0 , city.length -1)
    //     rebox.city = city
    //   } catch (error) {
    //     rebox.state = 'RJ'
    //   }

    //   await rebox.save()
    // }
    await Database.raw(`DELETE FROM rebox_prices WHERE true`)
    const states = STATES
    for (const state of states) {
      for (const city of state.cidades) {
        var reboxPrices = [

          //veiculos de passeio
          {
            'price' : 125.00,
            'km': 10,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 1,
            state: state.sigla,
            city
          },
          {
            'price' : 140.00,
            'km': 20,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 1,
            state: state.sigla,
            city
          },
          {
            'price' : 160.00,
            'km': 40,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 1,
            state: state.sigla,
            city
          },
          //Moto
          {
            'price' : 174.50,
            'km': 10,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 8,
            state: state.sigla,
            city
          },
          {
            'price' : 174.50,
            'km': 20,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 8,
            state: state.sigla,
            city
          },
          {
            'price' : 174.50,
            'km': 40,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 8,
            state: state.sigla,
            city
          },
          //Tricíclo
          {
            'price' : 174.50,
            'km': 10,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 2,
            state: state.sigla,
            city
          },
          {
            'price' : 174.50,
            'km': 20,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 2,
            state: state.sigla,
            city
          },
          {
            'price' : 174.50,
            'km': 40,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 2,
            state: state.sigla,
            city
          },
          //Quadricíclo
          {
            'price' : 174.50,
            'km': 10,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 3,
            state: state.sigla,
            city
          },
          {
            'price' : 174.50,
            'km': 20,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 3,
            state: state.sigla,
            city
          },
          {
            'price' : 174.50,
            'km': 40,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 3,
            state: state.sigla,
            city
          },
          //Vans
          {
            'price' : 210,
            'km': 10,
            'per_km': 1.50,
            'commission': 28.5,
            'port_car_id': 4,
            state: state.sigla,
            city
          },
          {
            'price' : 210,
            'km': 20,
            'per_km': 1.50,
            'commission': 28.5,
            'port_car_id': 4,
            state: state.sigla,
            city
          },
          {
            'price' : 210,
            'km': 40,
            'per_km': 1.50,
            'commission': 28.5,
            'port_car_id': 4,
            state: state.sigla,
            city
          },
          //Sedan Grande Porte
          {
            'price' : 125,
            'km': 10,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 5,
            state: state.sigla,
            city
          },
          {
            'price' : 140,
            'km': 20,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 5,
            state: state.sigla,
            city
          },
          {
            'price' : 160,
            'km': 40,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 5,
            state: state.sigla,
            city
          },
          //Pickup SUV
          {
            'price' : 125,
            'km': 10,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 6,
            state: state.sigla,
            city
          },
          {
            'price' : 140,
            'km': 20,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 6,
            state: state.sigla,
            city
          },
          {
            'price' : 160,
            'km': 40,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 6,
            state: state.sigla,
            city
          },
          //Utilitário Carga
          {
            'price' : 130,
            'km': 10,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 7,
            state: state.sigla,
            city
          },
          {
            'price' : 170,
            'km': 20,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 7,
            state: state.sigla,
            city
          },
          {
            'price' : 210,
            'km': 40,
            'per_km': 1.50,
            'commission': 40,
            'port_car_id': 7,
            state: state.sigla,
            city
          },
        ]
        await ReboxPrice.createMany(reboxPrices)
      }

    }

  }
}

module.exports = ReboxPriceSeeder
