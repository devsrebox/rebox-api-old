'use strict'

const { STATES } = require('./cities')

/*
|--------------------------------------------------------------------------
| StatusVehicleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const StatusVehicle = use('App/Models/StatusVehicle')
const Database = use('Database')
const Helpers = use('Helpers')
const fs = require('fs')

class StatusVehicleSeeder {
  async run () {
    await StatusVehicle.query().where('id', '>', 7).delete()
    await fs.readFile(Helpers.seedsPath('status_vehicles.sql'), "utf8", async function(err, data) {
      await Database.raw(data)
    });


  }
}

module.exports = StatusVehicleSeeder
