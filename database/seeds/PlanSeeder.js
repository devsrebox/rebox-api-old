'use strict'

/*
|--------------------------------------------------------------------------
| PlanSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Plan = use('App/Models/Plan')

class PlanSeeder {
  async run () {
    const plans = [
      {
        id: 1,
        description: 'Plano Rebox',
        price: 238.90,
        plots: 12,
        grace_period: 72,
        vehicle_year: 20,

      },
      {
        id: 2,
        description: 'Plano Rebox +',
        price: 358,
        plots: 12,
        grace_period: 0,
        vehicle_year: 0,

      }
    ]
    await Plan.createMany(plans)
  }
}

module.exports = PlanSeeder
