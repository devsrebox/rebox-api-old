'use strict'

/*
|--------------------------------------------------------------------------
| TypesServiceSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const TypesService = use('App/Models/TypesService')
class TypesServiceSeeder {
  async run () {
    await TypesService.query().delete()
    const types = [
      {
        'id': 1,
        'description': 'Reboque',
      },
      {
        'id': 2,
        'description': 'Chaveiro',
      },
      {
        'id': 3,
        'description': 'Socorro mecânico',
      },
      {
        'id': 4,
        'description': 'Troca de pneu',
      },
      {
        'id': 5,
        'description': 'Carga na bateria',
      },
      {
        'id': 6,
        'description': 'Carro reserva',
      },
      {
        'id': 7,
        'description': 'Pane seca',
      },
      {
        'id': 8,
        'description': 'Pane elétrica',
      },
      {
        'id': 9,
        'description': 'Assistência funerária',
      },
    ]

    await TypesService.createMany(types)
  }
}

module.exports = TypesServiceSeeder
