'use strict'

/*
|--------------------------------------------------------------------------
| StatusRemovalPlanSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const RemovalLocation = use('App/Models/RemovalLocation')
const { STATES } = require('./cities')
class StatusRemovalPlanSeeder {
  async run () {
    for (const state of STATES) {
      for (const city of state.cidades) {
        var list = [{
          description: 'Via pública',
          price: 0,
          state: state.sigla,
          city,
          location_id: 1,
          is_plan: true
        },
        {
          description: 'Ribanceira / Fora da via',
          price: 50,
          state: state.sigla,
          city,
          location_id: 2,
          is_plan: true
        },
        {
          description: 'Garagem subsolo',
          price: 0,
          state: state.sigla,
          city,
          location_id: 3,
          is_plan: true
        },
        {
          description: 'Garagem nível da rua',
          price: 0,
          state: state.sigla,
          city,
          location_id: 4,
          is_plan: true
        }
      ]
      await RemovalLocation.createMany(list)
      }
    }

  }
}

module.exports = StatusRemovalPlanSeeder
