'use strict'

/*
|--------------------------------------------------------------------------
| ServiceCostSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/


const { STATES } = require('./cities')
const ServiceCost = use('App/Models/ServiceCost')
class ServiceCostSeeder {
  async run() {
    const states = STATES
    for (const state of states) {
      for (const city of state.cidades) {
        var list = [
          {
            description: 'Reboque',
            cost: 0,
            city,
            state: state.sigla
          },
          {
            description: 'Chaveiro',
            cost: 0,
            city,
            state: state.sigla
          },
          {
            description: 'Socorro mecânico',
            cost: 0,
            city,
            state: state.sigla
          },
          {
            description: 'Troca de pneu',
            cost: 0,
            city,
            state: state.sigla
          },
          {
            description: 'Carga na bateria',
            cost: 0,
            city,
            state: state.sigla
          },
          {
            description: 'Carro reserva',
            cost: 0,
            city,
            state: state.sigla
          },
          {
            description: 'Pane seca',
            cost: 0,
            city,
            state: state.sigla
          },
          {
            description: 'Pane elétrica',
            cost: 0,
            city,
            state: state.sigla
          },
          {
            description: 'Assistência funerária',
            cost: 0,
            city,
            state: state.sigla
          },
        ]
        await ServiceCost.createMany(list)
      }
    }
  }
}

module.exports = ServiceCostSeeder
