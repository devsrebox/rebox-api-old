'use strict'

/*
|--------------------------------------------------------------------------
| ReboxSurplusSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const ReboxSurplus = use('App/Models/ReboxSurplus')
const { STATES } = require('./cities')
const Database = use('Database')
const Helpers = use('Helpers')
const fs = require('fs')
class ReboxSurplusSeeder {
  async run () {
    await Database.raw(`DELETE FROM rebox_surpluses WHERE true`)
    await fs.readFile(Helpers.seedsPath('rebox_surplus.sql'), "utf8", async function(err, data) {
      await Database.raw(data)
    });

  }
}

module.exports = ReboxSurplusSeeder
