"use strict";

/*
|--------------------------------------------------------------------------
| PlansDetailSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const PlansDetail = use("App/Models/PlansDetail");

class PlansDetailSeeder {
  async run() {
    const details = [
      {
        plan_id: 1,
        description: "Reboque em caso de pane/acidente",
        number_uses: 1,
        km_max: 100,
        cover_value: 0,
      },
      {
        plan_id: 1,
        description: "Socorro Mecânico",
        number_uses: 1,
        km_max: 100,
        cover_value: 0,
      },
      {
        plan_id: 1,
        description: "Recuperação do veículo em caso de acidente/pane",
        number_uses: 1,
        km_max: 100,
        cover_value: 0,
      },
      {
        plan_id: 1,
        description: "Troca de pneu",
        number_uses: 1,
        km_max: 0,
        cover_value: 200,
      },
      {
        plan_id: 1,
        description: "Chuveiro auto",
        number_uses: 1,
        km_max: 0,
        cover_value: 200,
      },
      {
        plan_id: 1,
        description: "Hospedagem",
        number_uses: 1,
        km_max: 0,
        cover_value: 100,
      },
      {
        plan_id: 1,
        description: "Carga na bateria",
        number_uses: 1,
        km_max: 0,
        cover_value: 0,
      },
      {
        plan_id: 2,
        description: "Reboque em caso de pane/acidente",
        number_uses: 1,
        km_max: 100,
        cover_value: 0,
      },
      {
        plan_id: 2,
        description: "Socorro Mecânico",
        number_uses: 1,
        km_max: 100,
        cover_value: 0,
      },
      {
        plan_id: 2,
        description: "Recuperação do veículo em caso de acidente/pane",
        number_uses: 1,
        km_max: 100,
        cover_value: 0,
      },
      {
        plan_id: 2,
        description: "Troca de pneu",
        number_uses: 1,
        km_max: 0,
        cover_value: 200,
      },
      {
        plan_id: 2,
        description: "Chuveiro auto",
        number_uses: 1,
        km_max: 0,
        cover_value: 200,
      },
      {
        plan_id: 2,
        description: "Hospedagem",
        number_uses: 1,
        km_max: 0,
        cover_value: 100,
      },
      {
        plan_id: 2,
        description: "Carga na bateria",
        number_uses: 1,
        km_max: 0,
        cover_value: 0,
      },
      {
        plan_id: 2,
        description: "Troca de óleo",
        number_uses: 1,
        km_max: 0,
        cover_value: 0,
      },
    ];
    await PlansDetail.createMany(details);
  }
}

module.exports = PlansDetailSeeder;
