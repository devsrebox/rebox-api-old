'use strict'

const { STATES } = require('./cities')

/*
|--------------------------------------------------------------------------
| StatusRemovalLocationSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Luclocation_id models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const RemovalLocation = use('App/Models/RemovalLocation')
const Database = use('Database')
const Helpers = use('Helpers')
const fs = require('fs')
class StatusRemovalLocationSeeder {
  async run () {
    await RemovalLocation.query().where('id', '>', 4).delete()
    await fs.readFile(Helpers.seedsPath('removal_name.sql'), "utf8", async function(err, data) {
      await Database.raw(data)
    });


  }
}

module.exports = StatusRemovalLocationSeeder
