"use strict"

const { STATES } = require('./cities')
const Database = use('Database')
const StatusVehicle = use('App/Models/StatusVehicle')
class StatusVehicleIsPlanSeeder {
  async run () {
    const states = STATES
    for (const state of states) {
      for (const city of state.cidades) {
        var prices = [
          {
            description: 'Parou de funcionar',
            price: 0,
            city,
            state: state.sigla,
            location_id: 1,
            is_plan: true
          },
          {
            description: 'Problema na roda',
            price: 0,
            city,
            state: state.sigla,
            location_id: 2,
            is_plan: true
          },
          {
            description: 'Câmbio travado',
            price: 0,
            city,
            state: state.sigla,
            location_id: 3,
            is_plan: true
          },
          {
            description: 'Capotado',
            price: 0,
            city,
            state: state.sigla,
            location_id: 4,
            is_plan: true
          },
          {
            description: 'Roda furtada',
            price: 0,
            city,
            state: state.sigla,
            location_id: 5,
            is_plan: true
          },
          {
            description: 'Nenhuma das opções',
            price: 0,
            city,
            state: state.sigla,
            location_id: 6,
            is_plan: true
          },
          {
            description: 'Veiculo blindado',
            price: 0,
            city,
            state: state.sigla,
            location_id: 7,
            is_plan: true
          },
        ]

        await StatusVehicle.createMany(prices)
      }
    }
  }
}

module.exports = StatusVehicleIsPlanSeeder
