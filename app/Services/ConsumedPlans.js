const { MAX_CONSUMED_PLANS } = require("../Utils/Constans");
const moment = use('moment')
const ConsumedPlan = use('App/Models/ConsumedPlan')
const UsersPlan = use('App/Models/UsersPlan')
const Contract = use('App/Models/Contract')
const consumedPlans = async (users_plan_id, from, user_id,   lng1 = null,
  lat1= null,
  lng2= null,
  lat2= null,) => {
  const detail = await UsersPlan.find(users_plan_id);


  const consumed = await ConsumedPlan.create({
    users_plan_id: detail.id,
    address: from, lng1, lat1, lng2,lat2,
  });

  const contract = await Contract.find(detail.contract_id)
  contract.uses++
  await contract.save()

  detail.uses -= 1;
  if (detail.uses == 0) {
    detail.is_active = false;
  }
  await detail.save();

  const contractIsValid = await UsersPlan.query()
    .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
    .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
    .where("uses", "=", 0)
    .where("user_id", user_id)
    .where('contract_id', contract.id)
    .fetch();

  if (contractIsValid && contractIsValid.rows.length >= MAX_CONSUMED_PLANS) {
    const servicesActives = await UsersPlan.query()
      .where("is_active", true)
      .where("user_id", user_id)
      .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
      .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
      .where('contract_id', contract.id)
      .where("uses", ">", 0)
      .fetch();
    for (const row of servicesActives.rows) {
      row.is_active = false;
      await row.save();
    }
  }

  return consumed
}

module.exports = { consumedPlans }
