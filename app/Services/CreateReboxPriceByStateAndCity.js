const ReboxPrice = use('App/Models/ReboxPrice')
const createReboxPrice = async (state, city) =>  {
  var registers = [{
    'price' : 125.00,
    'km': 10,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 1,
    state,
    city
  },
  {
    'price' : 140.00,
    'km': 20,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 1,
    state,
    city
  },
  {
    'price' : 160.00,
    'km': 40,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 1,
    state,
    city
  },
  //Moto
  {
    'price' : 174.50,
    'km': 10,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 8,
    state,
    city
  },
  {
    'price' : 174.50,
    'km': 20,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 8,
    state,
    city
  },
  {
    'price' : 174.50,
    'km': 40,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 8,
    state,
    city
  },
  //Tricíclo
  {
    'price' : 174.50,
    'km': 10,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 2,
    state,
    city
  },
  {
    'price' : 174.50,
    'km': 20,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 2,
    state,
    city
  },
  {
    'price' : 174.50,
    'km': 40,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 2,
    state,
    city
  },
  //Quadricíclo
  {
    'price' : 174.50,
    'km': 10,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 3,
    state,
    city
  },
  {
    'price' : 174.50,
    'km': 20,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 3,
    state,
    city
  },
  {
    'price' : 174.50,
    'km': 40,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 3,
    state,
    city
  },
  //Vans
  {
    'price' : 210,
    'km': 10,
    'per_km': 1.50,
    'commission': 28.5,
    'port_car_id': 4,
    state,
    city
  },
  {
    'price' : 210,
    'km': 20,
    'per_km': 1.50,
    'commission': 28.5,
    'port_car_id': 4,
    state,
    city
  },
  {
    'price' : 210,
    'km': 40,
    'per_km': 1.50,
    'commission': 28.5,
    'port_car_id': 4,
    state,
    city
  },
  //Sedan Grande Porte
  {
    'price' : 125,
    'km': 10,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 5,
    state,
    city
  },
  {
    'price' : 140,
    'km': 20,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 5,
    state,
    city
  },
  {
    'price' : 160,
    'km': 40,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 5,
    state,
    city
  },
  //Pickup SUV
  {
    'price' : 125,
    'km': 10,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 6,
    state,
    city
  },
  {
    'price' : 140,
    'km': 20,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 6,
    state,
    city
  },
  {
    'price' : 160,
    'km': 40,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 6,
    state,
    city
  },
  //Utilitário Carga
  {
    'price' : 130,
    'km': 10,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 7,
    state,
    city
  },
  {
    'price' : 170,
    'km': 20,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 7,
    state,
    city
  },
  {
    'price' : 210,
    'km': 40,
    'per_km': 1.50,
    'commission': 40,
    'port_car_id': 7,
    state,
    city
  }
]
await ReboxPrice.createMany(registers)
}

module.exports = {
  createReboxPrice
}
