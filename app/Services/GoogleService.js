"use strict";

const request = use("request");

class GoogleService {
  getDistanceKm(lat1, lng1, lat2, lng2) {
    return new Promise(async (resolve, reject) => {
      await request(
        `https://maps.googleapis.com/maps/api/distancematrix/json?&key=AIzaSyAAY_G1_D1yT5J7mzKqT54Jus3PfzjqoI0&origins=${lat1},${lng1}&destinations=${lat2},${lng2}`,
        function (err, res, body) {
          if (err) {
            reject("nok");
          }
          resolve(body);
        }
      );
    });
  }

  getAddress(lat1, lng1) {
    return new Promise(async (resolve, reject) => {
      await request(
        `https://maps.googleapis.com/maps/api/geocode/json?&key=AIzaSyAAY_G1_D1yT5J7mzKqT54Jus3PfzjqoI0&latlng=${lat1},${lng1}&sensor=false`,
        function (err, res, body) {
          if (err) {
            reject("nok");
          }
          resolve(body);
        }
      );
    });
  }
}

module.exports = GoogleService;
