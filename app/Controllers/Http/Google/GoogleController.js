'use strict'

const GoogleService = use('App/Services/GoogleService')

class GoogleController {

  async toFromGoogleApi({request, response}) {
    const { lat1, lng1, lat2, lng2 } = request.post()
    const objGoogleService = new GoogleService()
    await objGoogleService.getDistanceKm(lat1, lng1, lat2, lng2).then(
      data => {
        var json = JSON.parse(data)
        var from = json.origin_addresses[0]
        var to = json.destination_addresses[0]
        return response.status(200).send({
          message: 'dados retornados da api do google',
          from,
          to
        })
      }).catch((error) => console.log(error))

  }
}

module.exports = GoogleController
