"use strict";

const { PLAN_DETAIL_TRAILER_ID, USER_ROLE } = require("../../../Utils/Constans");
const moment = use('moment')
const Partner = use("App/Models/Partner");
const ServiceLocation = use("App/Models/ServiceLocation");
const UsersPlan = use("App/Models/UsersPlan");
const Mail = use("Mail");
const ReboxService = use("App/Models/ReboxService");
const Vehicle = use("App/Models/Vehicle");
const User = use("App/Models/User");
const RemovalLocation = use("App/Models/RemovalLocation");
const StatusVehicle = use("App/Models/StatusVehicle");
const ReboxPrice = use("App/Models/ReboxPrice");
const ReboxExtract = use("App/Models/ReboxExtract");
const CONSTANTS = use("App/Utils/Constans");
const GoogleService = use("App/Services/GoogleService");
const PortCar = use("App/Models/PortCar");
const ReboxSurplus = use("App/Models/ReboxSurplus");
const HistoryRequest = use("App/Models/HistoryRequest");
const Car = use("App/Models/Car");
const Ws = use("Ws");
const Env = use("Env");
class ReboxController {
  async index({ pagination }) {
    const results = await ReboxService.query()
      .with("partner", (builder) => {
        builder.with("user");
      })
      .with("vehicle", (builder) => {
        builder.with("user");
      })
      .has("payments")
      .paginate(pagination.page, pagination.perpage);

    return results;
  }

  async thereIsAnAttendant(state, city) {
    const location = await ServiceLocation.query()
      .where("state", state)
      .where("city", city)
      .first();

    return location ? true : false;
  }

  async store({ request, response, auth }) {
    const {
      cpf,
      email,
      name,
      cel,
      comments,
      removal_location_id,
      status_vehicle_id,
      attendant_id,
      scheduling_date,
      use_plan = false

    } = request.post();
    var { port_vehicle = 1 } = request.post();
    var findPayPlan = false
    var vehicleContract = false
    try {
      await auth.check()
      var authUser = await auth.getUser()
      if (authUser.role == USER_ROLE.ADMIN) {
        authUser = await User.findBy('email', email)
      }
      if (use_plan) {
        findPayPlan = await UsersPlan.query()
          .where("user_id", authUser.id)
          .where("is_active", true)
          .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
          .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
          .whereIn("plans_detail_id", PLAN_DETAIL_TRAILER_ID)
          .first();

        if (findPayPlan) {
          const findContract = await findPayPlan.contract().fetch()
          const vehicle = await findContract.vehicle().fetch()
          if (findContract.status != 'canceled') {
            const port = await Car.query('model', vehicle.model).where('brand', vehicle.brand).first()

            port_vehicle = port.port_car_id
            vehicleContract = vehicle
          }
        }
      }
    } catch (error) {
      console.log(error)
      console.log('Usuário não autenticado, seguindo o fluxo e reboque avulso')
    }


    let { password } = request.post();
    if (!password) password = email;

    const { lat1, lng1, lat2, lng2 } = request.post();

    const objGoogleService = new GoogleService();
    //Busca os kms percorridos baseado no maps do google e retonando um json com a distancia em milhar
    await objGoogleService
      .getDistanceKm(lat1, lng1, lat2, lng2)
      .then(async (data) => {
        var user = await User.findBy("email", email || authUser.email);

        if (!user)
          user = await User.create({ cpf, email, name, cel, password });
        user.merge({ name, cel });
        try {
          await user.save();
        } catch (error) {
          console.log("Não existem campos a serem atualizados", error);
        }
        var json = JSON.parse(data);

        var distanceKM = json.rows[0].elements[0].distance.value;
        var from = json.origin_addresses[0];
        var to = json.destination_addresses[0];
        var state = json.origin_addresses[0].split("-")[2].substr(1, 2);
        var city = json.origin_addresses[0]
          .split("-")[1]
          .split(",")[1]
          .substr(1);
        city = city.substr(0, city.length - 1);
        if (!(await this.thereIsAnAttendant(state, city))) {
          Mail.send("not_found_location", { state, city, user }, (message) => {
            message
              .to(Env.get("EMAIL_CONTACT"))
              .from(Env.get("MAIL_USERNAME"))
              .subject("Locais de atendimento");
          });
          return response.status(401).send({
            message: `Lamentamos, não há reboques disponíveis no momento para ${state} - ${city}`,
          });
        }
        const findLocation = await RemovalLocation.query()
          .where("location_id", removal_location_id || -1)
          .andWhere("state", state)
          .andWhere("city", city)
          .first();

        const findStatus = await StatusVehicle.query()
          .where("location_id", status_vehicle_id || -1)
          .andWhere("state", state)
          .andWhere("city", city)
          .first();

        const armored = await StatusVehicle.query()
          .where("location_id", CONSTANTS.ARMORED_ID)
          .andWhere("state", state)
          .andWhere("city", city)
          .first();

        console.log(
          "buscando um valor menor ou igual a " + (distanceKM * 2) / 1000
        );
        const roundTripKm = (distanceKM * 2) / 1000; // * 2 (id e volta)
        var reboxPrice = await ReboxPrice.query()
          .where("km", ">=", roundTripKm)
          .andWhere("state", state)
          .andWhere("city", city)
          .andWhere("port_car_id", port_vehicle)
          .andWhere("is_plan", use_plan)
          .orderBy("km", "asc")
          .first();
        if (!reboxPrice)
          reboxPrice = await ReboxPrice.query()
            .andWhere("state", state)
            .andWhere("city", city)
            .where("port_car_id", port_vehicle)
            .andWhere("is_plan", use_plan)
            .last();
        if (!reboxPrice) {
          console.log("Não encontrado preço");
          console.log(city);
          console.log(state);
          return response.status(404).send({
            message:
              "Preço não definido para cidade, entre em contato com a Rebox",
          });
        }

        const surplusPrice = await ReboxSurplus.query()
        .where('state', state)
        .where('city', city)
        .where('port_car_id', port_vehicle)
        .first()
        const { price, surplus } = CONSTANTS.REBOXVALUE(
          roundTripKm,
          reboxPrice.$attributes
        );
        const { reboxSurplus, partnerSurplus } = CONSTANTS.REBOXSURPLUS(
          surplus, surplusPrice.rebox_commission
        );
        var extraLocal = 0;
        var extraStatus = 0;
        var extraLocalRebox = 0;
        var extraLocalPartner = 0;
        var extraStatusRebox = 0;
        var extraStatusPartner = 0;
        var extraArmoredRebox = 0;
        var extraArmoredPartner = 0;
        var reboxExtra,
          partnerExtra = (0, 0);

        if (removal_location_id && findLocation.$attributes.price > 0) {
          extraLocal = findLocation.$attributes.price;
          var { reboxExtra, partnerExtra } = CONSTANTS.SERVICEEXTRA(
            findLocation.$attributes
          );
          console.log(reboxExtra, partnerExtra);

          extraLocalRebox = reboxExtra;
          extraLocalPartner = partnerExtra;
        }

        if (status_vehicle_id && findStatus.$attributes.price > 0) {
          extraStatus = findStatus.$attributes.price;
          var { reboxExtra, partnerExtra } = CONSTANTS.SERVICEEXTRA(
            findStatus.$attributes
          );
          extraStatusRebox = reboxExtra;
          extraStatusPartner = partnerExtra;
        }

        var { vehicle } = request.post();
        var findVehicle = false
        if (findPayPlan) {
          findVehicle = vehicleContract
        } else {
          findVehicle = await Vehicle.findOrCreate({
            brand: vehicle.brand,
            model: vehicle.model,
            board: vehicle.board,
            user_id: user.id,
            is_armored: vehicle.is_armored,
            color: vehicle.color || '',
            year: vehicle.year || 0,
          });
        }

        console.log(vehicle)


        var extraArmored = 0;

        if (findVehicle.is_armored == 1) {
          extraArmored = armored.$attributes.price;
          var { reboxExtra, partnerExtra } = CONSTANTS.SERVICEEXTRA(
            armored.$attributes
          );
          extraArmoredRebox = reboxExtra;
          extraArmoredPartner = partnerExtra;
        }
        console.log("porte do veiculo", port_vehicle);
        console.log("preço", price);
        console.log("excedente km", surplus);
        console.log("valor da comissão do excedente para rebox", reboxSurplus);
        console.log(
          "valor da comissão do excedente para parceiro",
          partnerSurplus
        );
        console.log("valor da comissão blindado para rebox", extraArmoredRebox);
        console.log(
          "valor da comissão blindado para parceiro",
          extraArmoredPartner
        );


        const reboxService = await ReboxService.create({
          status_payment: CONSTANTS.STATUS_PAYMENT["0"],
          comments,
          lat1,
          lng1,
          lat2,
          lng2,
          is_present: false,
          vehicle_id: findVehicle.id,
          removal_location_id,
          status_vehicle_id,
          amount: (
            price +
            surplus +
            extraLocal +
            extraStatus +
            extraArmored
          ).toFixed(2),
          from,
          to,
          status: "pendente",
          distance: roundTripKm,
          state,
          city,
          attendant_id,
          scheduling_date,
          port_car_id: port_vehicle,
        });
        /*  Verificando se o usuário solicitou o plano e se o plano está valido
              PLANO
        */


        if (findPayPlan) {
          reboxService.users_plan_id = findPayPlan.id
          if (roundTripKm < findPayPlan.km_max) {
            reboxService.amount = 0
          } else {
            const equivalOneKm = reboxService.amount / roundTripKm
            reboxService.amount = equivalOneKm * (roundTripKm - findPayPlan.km_max)
          }

          await reboxService.save()

        }


        /*
        ########
         Montanto o extrato
        #########

        */

        const {
          reboxCommission,
          partnerCommission,
        } = CONSTANTS.COMMISSIONVALUE(price, reboxPrice.commission);
        console.log("comissão do roque : rebox ", reboxCommission);
        console.log("comissão do roque : parceiro ", partnerCommission);
        const portCar = await PortCar.find(port_vehicle);
        await ReboxExtract.create({
          rebox_service_id: reboxService.id,
          description: `Reboque avulso, porte: ${portCar.port}`,
          value: price,
          commission_percent: reboxPrice.$attributes.commission,
          rebox_value: reboxCommission,
          partner_value: partnerCommission,
        });

        if (surplus > 0) {
          await ReboxExtract.create({
            rebox_service_id: reboxService.$attributes.id,
            description: "Excedente",
            value: surplus,
            commission_percent: CONSTANTS.SURPLUS,
            rebox_value: reboxSurplus,
            partner_value: partnerSurplus,
          });
        }
        if (removal_location_id && extraLocal > 0) {
          await ReboxExtract.create({
            rebox_service_id: reboxService.id,
            description:
              "Serv. Extra - " + findLocation.$attributes.description,
            value: findLocation.$attributes.price,
            commission_percent: CONSTANTS.EXTRA,
            rebox_value: extraLocalRebox,
            partner_value: extraLocalPartner,
          });
        }

        if (status_vehicle_id && extraStatus > 0) {
          await ReboxExtract.create({
            rebox_service_id: reboxService.id,
            description: "Serv. Extra - " + findStatus.$attributes.description,
            value: findStatus.$attributes.price,
            commission_percent: CONSTANTS.EXTRA,
            rebox_value: extraStatusRebox,
            partner_value: extraStatusPartner,
          });
        }
        if (findVehicle.is_armored == 1) {
          await ReboxExtract.create({
            rebox_service_id: reboxService.id,
            description: "Serv. Extra - " + armored.$attributes.description,
            value: armored.$attributes.price,
            commission_percent: CONSTANTS.EXTRA,
            rebox_value: extraArmoredRebox,
            partner_value: extraArmoredPartner,
          });
        }

        await HistoryRequest.create({
          user_id: user.id,
          from: reboxService.from,
          to: reboxService.to,
          audity_id: reboxService.id,
          description: 'Reboque em andamento',
          price: reboxService.amount,
          type: use_plan ? 'plano' : 'avulso'
        })

        return response.status(200).send({
          reboxService,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async update({ request, params }) {
    const { is_present, amount } = request.post();
    const reboxService = await ReboxService.findOrFail(params.id);
    const vehicle = await reboxService.vehicle().fetch();
    const user = await vehicle.user().fetch();
    if (is_present) {
      reboxService.is_present = is_present;
      reboxService.status = CONSTANTS.STATUS_SERVICE.ANDAMENTO;
    }

    if (amount && user.role == CONSTANTS.USER_ROLE.ADMIN)
      reboxService.amount = amount;
    await reboxService.save();
    const topic = Ws.getChannel("notifications").topic("notifications");
    if (topic) {
      topic.broadcast("new:rebox", reboxService);
    }
    return reboxService;
  }

  async updateSchedulingDateToRebox({ request, params }) {
    const { scheduling_date } = request.post();
    const rebox = await ReboxService.findOrFail(params.id);
    rebox.scheduling_date = scheduling_date;
    await rebox.save();
    return rebox;
  }

  async show({ params }) {
    await ReboxService.findOrFail(params.id);
    const results = await ReboxService.query()
      .where("id", params.id)
      .with("partner")
      .with("payments", (builder) => {
        builder.where("description", "pago");
      })
      .with("vehicle", (builder) => {
        builder.with("user");
      })
      .fetch();
    return results;
  }

  async requests({ params, pagination }) {
    const partner = await Partner.findOrFail(params.id);
    const serviceLocations = await partner.serviceLocations().fetch();

    const results = await ReboxService.query()
      .where(function () {
        this.whereIn("status_payment", ["pago", "pendente"]);
        this.where("status", "pendente");
        this.whereNull("attendant_id");
        this.orWhereHas("attendant", (builder) => {
          builder.whereNot("attendants.partner_id", params.id);
        });
      })
      .andWhere(function () {
        if (
          Array.isArray(serviceLocations.rows) &&
          serviceLocations.rows !== []
        ) {
          this.whereIn(
            "city",
            serviceLocations.rows.map((location) => location.city)
          );
          this.whereIn(
            "state",
            serviceLocations.rows.map((location) => location.state)
          );
        }
      })
      .orWhere("partner_id", params.id)
      .with("reject_partner", (builder) => {
        builder.where("partner_id", params.id);
      })
      .with("vehicle", (builder) => {
        builder.with("user");
      })
      .with("payments", (builder) => {
        builder.where("description", "pago");
      })
      .orderByRaw(
        `CASE
      WHEN status='${CONSTANTS.STATUS_SERVICE.PENDENTE}' THEN 1
      WHEN status='${CONSTANTS.STATUS_SERVICE.ANDAMENTO}' THEN 2
      WHEN status='${CONSTANTS.STATUS_SERVICE.ACEITO}' THEN 3
      WHEN status='${CONSTANTS.STATUS_SERVICE.FINALIZADO}' THEN 4
      END
      `
      )
      .orderBy("created_at", "desc")
      .paginate(pagination.page, pagination.perpage);

    return results;
  }
}

module.exports = ReboxController;
