'use strict'

const Assessment = use('App/Models/Assessment')
const ReboxService = use('App/Models/ReboxService')
const Ws = use('Ws')
const CONSTANTS = use('App/Utils/Constans')

class AssessmentsController {

  async store({ request, params }) {
    const { value, comments } = request.post()

    const rebox = await ReboxService.findOrFail(params.reboxid)
    const vehicle = await rebox.vehicle().fetch()
    const user = await vehicle.user().fetch()
    rebox.status = CONSTANTS.STATUS_SERVICE.FINALIZADO
    await rebox.save()
    const topic = Ws.getChannel('notifications').topic('notifications')
    if (topic) {
      topic.broadcast('change:rebox', rebox)
    }
    const assessments = await Assessment.findOrCreate({ 'rebox_service_id': params.reboxid, 'user_id': user.id })
    assessments.merge({ value, comments })
    await assessments.save()
    await assessments.load('rebox')
    return assessments

  }
}

module.exports = AssessmentsController
