"use strict";
const eRede = require("erede-node/lib/erede");
const Transaction = require("erede-node/lib/transaction");
const Store = require("erede-node/lib/store");
const Environment = require("erede-node/lib/environment");
const { STATUS_PAYMENT } = require("../../../../Utils/Constans");
const { consumedPlans } = require("../../../../Services/ConsumedPlans");
const ReboxService = use("App/Models/ReboxService");
const UsersPlan = use("App/Models/UsersPlan");
const Env = use("Env");
const Ws = use("Ws");
const CONSTANTS = use("App/Utils/Constans");

class PaymentsController {
  async store({ params, request, response }) {
    const {
      cred_number,
      cred_verification,
      cred_due_date_month,
      cred_due_date_year,
      cred_name,
      portion,
      payment_mode,
      type_payment_mode,
    } = request.post();

    const rebox = await ReboxService.findOrFail(params.reboxid);
    const vehicle = await rebox.vehicle().fetch();
    const user = await vehicle.user().fetch();

    const payment = await rebox.payments().create({
      description: "validando",
      payment_method: payment_mode || "credito",
      type_payment_mode,
    });

    if (payment_mode && payment_mode == "presencial") {
      payment.description = "pago";
      await payment.save();
      rebox.status_payment = CONSTANTS.STATUS_PAYMENT.PENDENTE;
      await rebox.save();
      if (rebox.users_plan_id) {
        await consumedPlans(rebox.users_plan_id, rebox.from, user.id )
        const topic = Ws.getChannel("notifications").topic("notifications");
        if (topic) {
          topic.broadcast("new:service", {});
        }
      }
      const topic = Ws.getChannel("notifications").topic("notifications");
      if (topic) {
        topic.broadcast("new:rebox", rebox);
      }
      return response.status(200).send({
        message: "Serviço autorizado",
      });
    }

    let store = new Store(Env.get("REDE_TOKEN"), Env.get("REDE_PV"));

    var transaction;
    if (payment_mode && payment_mode == "debito") {
      transaction = new Transaction(
        rebox.amount * 100,
        payment.id + "t" + Env.get("NODE_ENV")
      ).debitCard(
        cred_number,
        cred_verification,
        cred_due_date_month,
        cred_due_date_year,
        cred_name
      );
    } else {
      transaction = new Transaction(
        rebox.amount * 100,
        payment.id + "t" + Env.get("NODE_ENV"),
        portion
      ).creditCard(
        cred_number,
        cred_verification,
        cred_due_date_month,
        cred_due_date_year,
        cred_name
      );
    }

    await new eRede(store)
      .create(transaction)
      .then(async (transaction) => {
        if (transaction.returnCode === "00") {
          rebox.status_payment = CONSTANTS.STATUS_PAYMENT.PAGO;
          await rebox.save();
          payment.description = CONSTANTS.STATUS_PAYMENT.PAGO;
          await payment.save();

          const topic = Ws.getChannel("notifications").topic("notifications");
          if (topic) {
            topic.broadcast("new:rebox", rebox);
          }
          if (rebox.users_plan_id) {
            await consumedPlans(rebox.users_plan_id, rebox.from, user.id )
            const topic = Ws.getChannel("notifications").topic("notifications");
            if (topic) {
              topic.broadcast("new:service", {});
            }
          }
          return response.status(200).send({
            message: "Pagamento efetuado",
          });
        } else {
          console.log(
            "erro ao efetuar pagamento cod : " + transaction.returnCode
          );
          console.log("erro ao efetuar pagamento cod : " + transaction);
        }
      })
      .catch(async (error) => {
        rebox.status_payment = STATUS_PAYMENT.NAO_AUTORIZADO;
        await rebox.save();
        payment.description = STATUS_PAYMENT.NAO_AUTORIZADO;
        await payment.save();
        return response.status(401).send(error);
      });
  }
}

module.exports = PaymentsController;
