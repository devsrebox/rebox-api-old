'use strict'

const Mail = use('Mail')
const Env = use('Env')
class ContactController {
  async sendContactRebox({request}) {
    const {name, email, cel, message} = request.post()
    await Mail.send('contact', {name, cel, message, email}, (message) => {
      message
        .to(Env.get('EMAIL_CONTACT'))
        .from('noreplay@rebox.com.br')
        .subject('Fale conosco - Novo contato')
    })
  }

  async sendContactBoleto({request}) {
    const { name, email, cpf, planValue } = request.post()
    await Mail.send('boleto', {name, email, cpf, planValue}, (message) => {
      message
        .to('boletos@rebox.com.br')
        .from('noreplay@rebox.com.br')
        .subject(`NOVO CONTATO: ${name}`)
    })
  }
}


module.exports = ContactController
