'use strict'

const { LOGO_URL } = require("../../../Utils/Constans")

const User = use('App/Models/User')
const Partner = use('App/Models/Partner')
const Vehicle = use('App/Models/Vehicle')
const TypesService = use('App/Models/TypesService')
const Database = use('Database')
const CONSTANTS = use('App/Utils/Constans')
const resetedPassword = use('App/Models/ResetedPassword')
const Mail = use('Mail')
const moment = use('moment')
const Env = use('Env')
const Helpers = use('Helpers')
class AuthController {
  async register({ request, response, auth }) {

    const trx = await Database.beginTransaction()

    try {
      const { name, address_zip_code, address_neighborhood , address_street, email, password, role, cel,
        types, cnpj, cpf, serviceLocations, vehicle, location } = request.all()

      const findUser = await User.findBy('email', email)

      if (cnpj) {
        const findByCnpj = await Partner.findBy('cnpj', cnpj)
        if (findByCnpj) {
          return response.status(409).send({ message: 'cnpj já cadastrado' })
        }

      }
      if (findUser) {
        return response.status(409).send({ message: 'email já cadastrado' })
      }

      const user = await User.create({ name, email, password, cel, role, cpf, address_zip_code, address_neighborhood , address_street,  }, trx)
      // commita a transaction
      await trx.commit()

      if (user.role == CONSTANTS.USER_ROLE.PARTNER) {
        const partner = await user.partner().create({ cnpj, name, address: location })
        if (types) {
          await partner.types().detach()
          await partner.types().attach(types)
        }
        if (serviceLocations) {
          await partner.serviceLocations().createMany(serviceLocations)
        }

        const typesDescription = await TypesService.query().whereIn('id', types).fetch()
        var link_add = `${Env.get('APP_URL') }partners/accept/${partner.id}`
        var link_remove = `${Env.get('APP_URL') }partners/block/${partner.id}`
        var logo_url =  LOGO_URL
        await Mail.send('permission', {typesDescription, logo_url, user, link_add, link_remove, cnpj, date: moment(user.created_at).format('DD/MM/yyyy'), hour:  moment(user.created_at).format('HH:mm') }, (message) => {
          message
          .to(Env.get('EMAIL_CONTACT'))
          .from('noreplay@rebox.com.br')
          .subject('Fale conosco - Pedido de parceria')
        })
      }
      if (user.role == CONSTANTS.USER_ROLE.USER && vehicle) {
        var findVehicle = await Vehicle.findOrCreate({
          user_id: user.id,
          brand: vehicle.brand,
          model: vehicle.model,
        });
        findVehicle.merge({
          year: vehicle.year,
          color: vehicle.color,
          board: vehicle.board,
          is_armored: vehicle.is_armored,
          motor: vehicle.motor,
          oil_changes: vehicle.oil_changes,
        });
        await findVehicle.save();
        await user.load('vehicles')
      }
      let data = await auth.withRefreshToken().attempt(email, password)

      return response.status(201).send({
        message: 'Usuário cadastrado com sucesso!',
        user: user,
        data
      })
    } catch (e) {
      await trx.rollback()
      return response.status(400).send({
        error: 'Erro ao realizar cadastro',
        message: e.message
      })
    }
  }



  async login({ request, response, auth }) {

    const { cpf, password } = request.only(['cpf', 'password'])

    let user = await User.query()
      .where('cpf', cpf)
      .first();

    if (!user) return response.status('404').send({ message: "CPF e/ou senha inválidos." });

    let data = await auth.withRefreshToken().attempt(user.email, password);

    return response.send({ data, user })
  }

  async loginPartner({ request, response, auth }) {

    const { cnpj, password } = request.only(['cnpj', 'password'])

    const userPartner = await Partner.findByOrFail('cnpj', cnpj)
    if (userPartner.status == false) {
      return response.status(401).send({
        message: 'Aguardando a aprovação da rebox'
      })
    }
    const userAux = await User.findOrFail(userPartner.user_id)

    let data = await auth.withRefreshToken().attempt(userAux.email, password)

    const user = await User.findByOrFail('email', userAux.email)
    await user.load('partner')

    return response.send({ data, user })
  }

  async loginAttendant({ request, response, auth}) {
    const { cpf, password } = request.only(['cpf', 'password'])

    let user = await User.query()
      .where('cpf', cpf)
      .with('attendant', (builder) => {
        builder.with('partner', (builder) => {
          builder.with('user')
        })
      })
      .first();

    if (!user) return response.status('404').send({ message: "CPF e/ou senha inválidos." });

    let data = await auth.withRefreshToken().attempt(user.email, password);

    return response.send({ data, user })
  }

  async loginAdmin({ request, response, auth}) {
    const { email, password } = request.only(['email', 'password'])

    const user = await User.findByOrFail('email', email)

    if (user.role != CONSTANTS.USER_ROLE.ADMIN)
      return response.status(401).send({
        message: 'função disponível apenas para administradores'
      })

    let data = await auth.withRefreshToken().attempt(email, password)


    return response.send({ data, user })
  }

  async forgotPassword({ request, response }) {
    try {

      var { cnpj_email } = request.post()
      var email = cnpj_email
      if (cnpj_email) {
        const partner = await Partner.findBy('cnpj', cnpj_email)
        if (partner) {
          const userPartner = await partner.user().fetch()
          email = userPartner.email
        }
      }

      const user = await User.findBy('email', email)

      if (!user)
        response.status(404).send({ message: 'Email não encontrado.' })

      const resetedInfo = await resetedPassword.create({ email });

      const id = resetedInfo.id;
      const max = 6 - id.toString().length;
      let code = Math.random() * 1000000
      code = code.toString();
      code = code.substr(0, max);
      code = `${id}${code}`

      resetedInfo.code = code;

      await resetedInfo.save();

      await Mail.send('forgotpassword', { code }, (message) => {
        message
          .to(user.email)
          .from('noreplay@rebox.com.br')
          .subject('Código para redefinir sua senha')
      })

      return { message: 'Email enviado com sucesso.', data: resetedInfo }
    }
    catch (error) {
      return error.message
    }
  }
  async changePassword({ request, auth, response }) {
    const user = await auth.getUser()
    user.merge(request.only(['password']))
    await user.save()
    return response.status(200).send({
      message: 'Senha alterada com sucesso'
    })


  }
  async validateCode({ request, response }) {
    const { code, email } = request.only(['code', 'email'])

    const requested = await resetedPassword.query()
      .where('code', code)
      .andWhere('email', email)
      .first();

    if (!requested)
      return response.status(404).send({ message: 'Código e/ou email inválido' })

    const limitTime = moment(requested.created_at).add(10, 'minutes');

    if (moment() > limitTime)
      return response.status(401).send({ message: 'Link expirado.' })

    return requested
  }


  async resetPassword({ auth, request, params, response }) {

    const { password, id } = request.only(['password', 'id']);

    const requested = await resetedPassword.find(id);

    if (!requested)
      return response.status(404).send({ message: 'Código e/ou email inválido' })

    if (requested.status == 0)
      return response.status(404).send({ message: 'Código já utilizado' })

    const user = await User.query()
      .where('email', requested.email)
      .first()

    user.password = password
    await user.save()

    const data = await auth.attempt(user.email, password)

    requested.status = 0;
    await requested.save();

    return { data, user };
  }
}

module.exports = AuthController
