'use strict'

const HistoryRequest = use('App/Models/HistoryRequest')
const User = use('App/Models/User')
const Database = use('Database')
class HistoryRequestsController {
  async index({ params }) {
    const user = await User.findOrFail(params.userid)
    const results = await HistoryRequest.query()
      .where('user_id', user.id)
      .with('partner', (builder) => {
        builder.with('user')
      })
      .orderBy('id', 'desc')
      .limit(5)
      .fetch()
    var result = []
    results.rows.map((row, index) => {
      if (index > 0) {
        if (row.audity_id != results.rows[index - 1].audity_id)
        result.push(row)
      }else {
        result.push(row)
      }

    })

    return result
  }
}


module.exports = HistoryRequestsController
