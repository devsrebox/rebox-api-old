'use strict'
const User = use('App/Models/User')

class VehiclesController {

  async index({params, auth}) {
    const user = await User.findOrFail(params.userid)

    const vehicles = await user.vehicles().fetch()

    return vehicles
  }
  async store({params, auth, request}) {
    const user = await User.findOrFail(params.userid)

    const {vehicle} = request.post()


    const result = await user.vehicles().create(
      vehicle
    )

    return result
  }

}

module.exports= VehiclesController
