"use strict";
const eRede = require("erede-node/lib/erede");
const Transaction = require("erede-node/lib/transaction");
const Store = require("erede-node/lib/store");
const Environment = require("erede-node/lib/environment");
const Ws = use("Ws");
const Mail = use("Mail");
const {
  STATUS_PAYMENT,
  MAX_CONSUMED_PLANS,
  LACK_OF_THE_PLAN,
} = require("../../../../Utils/Constans");
const { consumedPlans } = require("../../../../Services/ConsumedPlans");
const Env = use("Env");
const Plan = use("App/Models/Plan");
const HistoryRequest = use("App/Models/HistoryRequest");
const User = use("App/Models/User");
const PaymentsPlan = use("App/Models/PaymentsPlan");
const UsersPlan = use("App/Models/UsersPlan");
const Contract = use("App/Models/Contract");
const moment = use("moment");
class PlansController {
  async index({ auth, response }) {
    const user = await auth.getUser();
    const results = await UsersPlan.query()
      .where("is_active", true)
      .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
      .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
      .where("uses", ">", 0)
      .whereHas('contract', (builder) => {
        builder.where('status', 'sold')
      })
      .where("user_id", user.id)
      .fetch();

      if (results.rows.length > 0) {
        const contract = await results.rows[0].contract().fetch()
        if (contract.status == "canceled") {
          return response.status(401).send({
            message: `Contrato cancelado!`
          })
        }
        if (moment(contract.created_at).add(LACK_OF_THE_PLAN, 'hours') > moment()) {

          return response.status(401).send({
            message: `Parabéns! Você acaba de adquirir um plano de assistência veicular 24 horas da Rebox`
          })
        }
      }

    return results;
  }

  async last({params}) {
    const user = await User.findOrFail(params.userid)
    const results = await UsersPlan.query()
    .with('contract')
    .orderBy('id', 'asc')
    .limit(1)
    .where("user_id", user.id)
    .first();

    return results
  }


  async isActive({ params, response }) {
    const user = await User.findByOrFail('email', params.email)
    const findPayPlan = await Contract.query()
      .where("uses", '<=', MAX_CONSUMED_PLANS)
      .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
      .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
      .where('user_id', user.id)
      .with('use_plans', (builder) => {
        builder.where('is_active', true)
      })
      .first();


      if (findPayPlan) {
        const contract = findPayPlan
        if (contract.status == "canceled") {
          return response.status(401).send({
            message: `Contrato cancelado!`
          })
        }
        if (moment(contract.created_at).add(LACK_OF_THE_PLAN, 'hours') > moment()) {
          return response.status(401).send({
            message: `Parabéns! Você acaba de adquirir um plano de assistência veicular 24 horas da Rebox`
          })
        }
      }

    if (findPayPlan) {
      return response.status(200).send({
        message: 'plano ativo',
        details: findPayPlan,
        user
      })
    } else {
      return response.status(200).send({
        message: 'Usuário não possui um plano ativo',
        details: [],
        user
      })
    }
  }

  async paymentPlan({ auth, params, request, response }) {
    const {
      cred_number,
      cred_verification,
      cred_due_date_month,
      cred_due_date_year,
      cred_name,
      portion,
      payment_mode,
      vehicle_id
    } = request.post();

    const user = await auth.getUser();

    const plan = await Plan.findOrFail(params.id);

    const findPayPlan = await UsersPlan.query()
      .where("user_id", user.id)
      .where("is_active", true)
      .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
      .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
      .first();

    if (findPayPlan) {
      return response.status(401).send({
        message: "O usuário já possui um plano ativo",
      });
    }

    const payment = await PaymentsPlan.create({
      plan_id: plan.id,
      user_id: user.id,
      method_payment: payment_mode,
      status_code: "pendente",
    });

    let store = new Store(Env.get("REDE_TOKEN"), Env.get("REDE_PV"));

    var transaction;
    if (payment_mode && payment_mode == "debito") {
      transaction = new Transaction(
        plan.price * 100,
        payment.id + "-plan" + Env.get("NODE_ENV")
      ).debitCard(
        cred_number,
        cred_verification,
        cred_due_date_month,
        cred_due_date_year,
        cred_name
      );
    } else {
      transaction = new Transaction(
        plan.price * 100,
        payment.id + "-plan" + Env.get("NODE_ENV"),
        portion
      ).creditCard(
        cred_number,
        cred_verification,
        cred_due_date_month,
        cred_due_date_year,
        cred_name
      );
    }

    await new eRede(store)
      .create(transaction)
      .then(async (transaction) => {
        payment.status_code = transaction.returnCode;
        payment.status_details = transaction.returnMessage;
        await payment.save();
        if (transaction.returnCode === "00") {
          const topic = Ws.getChannel("notifications").topic("notifications");
          if (topic) {
            topic.broadcast("new:service", {});
          }
          const details = await plan.details().fetch();
          const contract = await Contract.create({
            validity_of: moment(),
            validity_until: moment().add(1, "year"),
            uses: 0,
            user_id: user.id,
            plan_id: plan.id,
            vehicle_id,
            price: plan.price
          })
          for (const row of details.rows) {
            await UsersPlan.create({
              user_id: user.id,
              plans_detail_id: row.id,
              description: row.description,
              validity_of: moment(),
              validity_until: moment().add(1, "year"),
              method_payment: payment_mode,
              km_max: row.km_max,
              uses: row.number_uses,
              cover_value: row.cover_value,
              is_active: true,
              contract_id: contract.id,
              price_pg: plan.price
            });
          }
          return response.status(200).send({
            message: "Pagamento efetuado",
          });
        } else {
          console.log(
            "erro ao efetuar pagamento cod : " + transaction.returnCode
          );
          console.log("erro ao efetuar pagamento cod : " + transaction);
        }
      })
      .catch(async (error) => {
        payment.status = STATUS_PAYMENT.NAO_AUTORIZADO;
        await payment.save();
        return response.status(401).send(error);
      });
  }

  async consumePlan({ params, response, auth, request }) {
    const { address, lng1, lat1, lng2, lat2 } = request.post();
    const user = await auth.getUser();

    const detail = await UsersPlan.findOrFail(params.id);

    const isValidUserPlan = await UsersPlan.query()
      .where("id", detail.id)
      .where("is_active", true)
      .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
      .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
      .where("uses", ">", 0)
      .where("user_id", user.id)
      .first();
    if (!isValidUserPlan)
      return response.status(401).send({
        message: "Esse serviço encontra-se inativo ou fora da validade",
      });
    const consumed = await consumedPlans(detail.id, address, user.id,  lng1, lat1, lng2,lat2,)

    await HistoryRequest.create({
      user_id: user.id,
      from: address,
      to: '',
      audity_id: consumed.id,
      description: detail.description,
      price: detail.price_pg,
      type: 'plano'
    })

    const topic = Ws.getChannel("notifications").topic("notifications");
    if (topic) {
      topic.broadcast("new:service", {});
    }

    return response.status(201).send({
      message:
        "Sua solicitação foi enviada para equipe interna da Rebox, aguarde o retorno",
    });
  }

  async cancel({params}) {
    const contract = await Contract.findOrFail(params.id)
    const user = await contract.user().fetch()
    contract.status = 'canceled'
    await contract.save()
    await Mail.send('cancel_contract', {user, contract, date: moment().format('DD/MM/YYYY H:mm')}, (message) => {
      message
      .to(Env.get('EMAIL_CONTACT'))
      .from('noreplay@rebox.com.br')
      .subject('Contratos - pedido de cancelamento')
    })
  }
}

module.exports = PlansController;
