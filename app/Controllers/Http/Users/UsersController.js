"use strict";

const { MAX_CONSUMED_PLANS } = require("../../../Utils/Constans");
const moment = require('moment')

const User = use("App/Models/User");
const Vehicle = use("App/Models/Vehicle");
const Drive = use("Drive");
const CONSTANTS = use("App/Utils/Constans");

class UsersController {
  async update({ request, auth, response, params }) {
    const user = await User.findOrFail(params.id);
    const {
      email,
      cpf,
      name,
      cel,
      tel,
      city,
      state,
      vehicles,
    } = request.post();
    const userExist = await User.findBy("email", email || "te@te");
    if (userExist && user.email != userExist.email) {
      return response.status(409).send({
        message: "Email já cadastrado, dados parcialmente salvos!",
      });
    }
    user.merge({ email, cpf, name, cel, tel, city, state });
    await user.save();
    if (vehicles) {
      for (const vehicle of vehicles) {
        var findVehicle = await Vehicle.findOrCreate({
          user_id: user.id,
          brand: vehicle.brand,
          model: vehicle.model,
        });
        findVehicle.merge({
          year: vehicle.year,
          board: vehicle.board,
          is_armored: vehicle.is_armored,
          motor: vehicle.motor,
          color: vehicle.color,
          oil_changes: vehicle.oil_changes,
        });
        await findVehicle.save();
        await user.load("vehicles");
      }
    }
    await user.load("vehicles");

    return response.status(200).send({
      message: "Dados atualizados com sucesso!",
      user,
    });
  }

  async changePassword({ request, response, params }) {
    const user = await User.findOrFail(params.id);
    user.merge(request.only(["password"]));
    await user.save();
    return response.status(200).send({
      message: "Senha alterada com sucesso",
    });
  }
  async show({ params }) {
    const user = await User.findOrFail(params.id);
    await user.loadMany({
      vehicles: null,
      contract: (builder) =>
        builder
          .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
          .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
          .where("status", "sold")
          .where("uses", "<", MAX_CONSUMED_PLANS),
    });

    return user;
  }

  async getUserById({ request }) {
    const { email } = request.post();
    await User.findByOrFail("email", email);
  }

  async updateImgProfile({ request, response, auth }) {
    const user = await auth.getUser();

    const validationOptions = {
      types: ["jpeg", "jpg", "png"],
      size: "2mb",
    };
    if (user.img_profile) {
      const oldKey = user.img_profile.split("/");

      await Drive.delete(
        oldKey[oldKey.length - 2] + "/" + oldKey[oldKey.length - 1]
      );
    }

    request.multipart.file("profile", validationOptions, async (file) => {
      // set file size from stream byteCount, so adonis can validate file size
      file.size = file.stream.byteCount;

      // catches validation errors, if any and then throw exception
      const error = file.error();
      if (error.message) {
        throw new Error(error.message);
      }
      const Key = `profile/${file.clientName}`;
      const ContentType = file.headers["content-type"];
      const ACL = "public-read";
      // upload file to s3
      const path = await Drive.put(Key, file.stream, {
        ContentType,
        ACL,
      });

      user.img_profile = path;
      await user.save();
    });
    await request.multipart.process();
    return response.status(200).send({
      message: "Foto de perfil atualizada com sucesso",
      data: user,
    });
  }
}

module.exports = UsersController;
