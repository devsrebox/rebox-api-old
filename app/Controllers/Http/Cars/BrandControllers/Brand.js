'use strict'

const Car = use('App/Models/Car')
const Env = use('Env')
const Database = use('Database')

class BrandController {
    async index({ request, response }) {
        try {
            const db = Env.get('DB_CONNECTION');

            const { brand } = request.only(['brand']);

            let brands = null;

            if (db == 'mysql') {
                brands = await Car.query()
                .select('cars.brand','cars.port_car_id')
                .where('cars.brand', 'like', `${brand}%`)
                .orderBy('cars.brand', 'ASC')
                .groupBy('cars.brand')
                .fetch()
            } else if (db == 'pg') {
                brands = await Database.raw(`SELECT distinct on (brand) brand, port_car_id FROM cars WHERE brand ilike '${brand}%' ORDER BY brand`)
                .then(result => brands = result.rows)
            }
            else {
                brands = await Car.query()
                .select('cars.brand','cars.port_car_id')
                .select('cars.brand','cars.port_car_id')
                .groupBy('cars.brand')
                .orderBy('cars.brand', 'ASC')
                .fetch()
            }

            return brands
        }
        catch(error) {
            return response.status(500).send({message: error.message});
        }
    }
}

module.exports = BrandController
