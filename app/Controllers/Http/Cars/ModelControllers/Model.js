'use strict'

const Car = use('App/Models/Car')
const Env = use('Env')
const Database = use('Database')

class ModelController {
    async index({ request, response }) {
        try {
            const { model, brand } = request.only(['model', 'brand']);
            const db = Env.get('DB_CONNECTION');
            let models = null;

            if (db == 'mysql') {
                models = await Car.query()
                .select('cars.brand','cars.port_car_id')
                .where('model', 'like', `${model}%`)
                .andWhere('brand', 'like', `${brand}`)
                .groupBy('cars.brand')
                .orderBy('model', 'ASC')
                .fetch()
            }
            else if (db == 'pg') {
              models = await Database.raw(
                `SELECT distinct on (model) model, brand, port_car_id FROM cars WHERE brand ilike '${brand}%' AND model ilike '${model}%' ORDER BY model`
                ).then(result => models = result.rows)
            }
            else {
                models = await Car.query()
                .select('cars.brand','cars.port_car_id')
                .where('model', `${model}%`)
                .andWhere('cars.brand', `${brand}`)
                .orderBy('model', 'ASC')
                .groupBy('brand')
                .fetch()
            }

            return models

        }
        catch(error) {
            return response.status(500).send({message: error.message});
        }
    }
}

module.exports = ModelController
