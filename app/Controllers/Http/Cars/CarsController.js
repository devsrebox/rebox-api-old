'use strict'

const Car = use('App/Models/Car')
const ReboxService = use('App/Models/ReboxService')

class CarsController {

  async index({ request }) {
    const { car } = request.get()
    const results = await Car.query()
    .where('model', 'LIKE', `${car}%`)
    .orWhere('brand', 'LIKE', `${car}%`)
    .orWhere('brand', 'LIKE', `%${car}%`)
    .fetch()

    return results
  }

  async normalize() {
    const reboxs = await ReboxService.query().select('rebox_services.id as serviceid')
    .select('vehicles.*').
    join('vehicles', 'vehicles.id', 'rebox_services.vehicle_id')
    .whereNull('rebox_services.port_car_id')
    .fetch()

    for (const rebox of reboxs.rows) {

      const port = await Car.query().where('model', rebox.model).where('brand', rebox.brand).first()
      const findRebox = await ReboxService.find(rebox.serviceid)
      if (!port) {
        findRebox.port_car_id = 1
      } else {
        findRebox.port_car_id = port.port_car_id
      }
      await findRebox.save()
    }
  }

}

module.exports = CarsController
