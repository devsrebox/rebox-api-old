'use strict'

const { MAX_CONSUMED_PLANS, PLAN_DETAIL_TRAILER_ID } = require("../../../../../Utils/Constans")

const UsersPlan = use('App/Models/UsersPlan')
const ConsumedPlan = use('App/Models/ConsumedPlan')
const User = use('App/Models/User')
const Contract = use('App/Models/Contract')
const Ws = use('Ws')
const moment = use('moment')
class ConsumedPlansController {
  async index({ params }) {
    const user = await User.findOrFail(params.userid)
    const results = await UsersPlan.query()
      .where("is_active", true)
      .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
      .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
      .where("uses", ">", 0)
      .whereHas('contract', (builder) => {
        builder.where('status', 'sold')
      })
      .whereNotIn('plans_detail_id', PLAN_DETAIL_TRAILER_ID)
      .where("user_id", user.id)
      .fetch();

    return results;
  }

  async consumePlan({ params, response, request }) {
    const { address, lng1, lat1, lat2, lng2 } = request.post();
    const user = await User.findOrFail(params.userid)
    const detail = await UsersPlan.findOrFail(params.id);

    const isValidUserPlan = await UsersPlan.query()
      .where("id", detail.id)
      .where("is_active", true)
      .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
      .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
      .where("uses", ">", 0)
      .where("user_id", user.id)
      .first();
    if (!isValidUserPlan)
      return response.status(401).send({
        message: "Esse serviço encontra-se inativo ou fora da validade",
      });
    const contract = await Contract.findOrFail(isValidUserPlan.contract_id)
    if (contract.status == 'canceled') {
      return response.status(401).send({
        message: "Contrato encontra-se cancelado",
      });
    }
    await ConsumedPlan.create({
      users_plan_id: detail.id,
      address: address || "N/A",
      lng1,
      lat1,
      lng2,
      lat2,
    });


    contract.uses++
    await contract.save()

    isValidUserPlan.uses -= 1;
    if (isValidUserPlan.uses == 0) {
      isValidUserPlan.is_active = false;
    }
    await isValidUserPlan.save();

    const contractIsValid = await UsersPlan.query()
      .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
      .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
      .where("uses", "=", 0)
      .where("user_id", user.id)
      .where('contract_id', contract.id)
      .fetch();

    if (contractIsValid && contractIsValid.rows.length >= MAX_CONSUMED_PLANS) {
      const servicesActives = await UsersPlan.query()
        .where("is_active", true)
        .where("user_id", user.id)
        .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
        .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
        .where('contract_id', contract.id)
        .where("uses", ">", 0)
        .fetch();
      for (const row of servicesActives.rows) {
        row.is_active = false;
        await row.save();
      }
    }
    const topic = Ws.getChannel("notifications").topic("notifications");
    if (topic) {
      topic.broadcast("new:service", {});
    }

    return response.status(201).send({
      message:
        "Sua solicitação foi enviada para equipe interna da Rebox, aguarde o retorno",
    });
  }
}

module.exports = ConsumedPlansController
