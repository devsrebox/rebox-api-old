"use strict";

const { createReboxPrice } = require("../../../../Services/CreateReboxPriceByStateAndCity");

const ReboxPrice = use("App/Models/ReboxPrice");
const ReboxSurplus = use("App/Models/ReboxSurplus");
const RemovalLocation = use("App/Models/RemovalLocation");
const StatusVehicle = use("App/Models/StatusVehicle");
const ServiceCost = use("App/Models/ServiceCost");
const Database = use('Database')
class PricesController {
  async getAllPricesGrouByCities({ pagination, request }) {
    const { search } = request.get()

    const results = await Database.select('*').from(function () {
      this.distinct('city')
        .distinct('state').from('rebox_prices')
        .where(function () {
          if (search) {
            this.whereRaw(`UPPER(state) like '${search.toUpperCase()}%'`)
            this.orWhereRaw(`UPPER(city) like '${search.toUpperCase()}%'`)
          }
        })

        .orderBy('state')
        .as('t1')
    })
      .paginate(pagination.page, pagination.perpage);

    return results;
  }

  async index({ request }) {
    const { state, city, is_plan } = request.get();

    var results = await ReboxSurplus.query()
      .where(function () {
        if (state) this.where("state", state);
        if (city) this.where("city", city);
        if (is_plan == 'true') this.where("port_car_id", 1)
      })
      .with('port')
      .orderBy('port_car_id')
      .fetch();

      for (const row of results.rows) {
        row['prices'] = await ReboxPrice.query()
        .where('state', row.state)
        .where('city', row.city)
        .where("is_plan", is_plan)
        .where('port_car_id', row.port_car_id).fetch()
      }
    if (results.rows.length == 0 && city && state) {
      await createReboxPrice(state, city)
      results = await ReboxPrice.query()
      .where(function () {
        if (state) this.where("state", state);
        if (city) this.where("city", city);
      })
      .where("is_plan", is_plan)
      .orderBy("port_car_id")
      .orderBy("km")
      .fetch();
    }

    var extras

    if (is_plan == 'true')  {

      extras = await ServiceCost.query().where(function () {
        if (state) this.where("state", state);
        if (city) this.where("city", city);
      }).fetch()
      extraResults = extras
    } else {
      console.log(is_plan)
      extras = await StatusVehicle.query().where(function () {
        if (state) this.where("state", state);
        if (city) this.where("city", city);
        this.where("is_plan", false)

      })
        .fetch()
      var moreExtra = await RemovalLocation.query().where(function () {
        if (state) this.where("state", state);
        if (city) this.where("city", city);
        this.where("is_plan", false)

      })
        .fetch()
      var extraResults = []
      for (const row of extras.rows) {
        extraResults.push(row.$attributes)
      }
      for (const row of moreExtra.rows) {
        extraResults.push(row.$attributes)
      }
    }



    return { ports: results, 'extras': extraResults };
  }

  async update({ request }) {
    var results = [];
    const { ports, extras } = request.post();
    var city, state = ''
    for (const port of ports) {
      var reboxSurplus = await ReboxSurplus.find(port.id)
      var { rebox_commission } = port;
      reboxSurplus.merge({rebox_commission})
      await reboxSurplus.save()
      console.log(reboxSurplus.id)
      for (const reboxPrices of port.prices) {
        var reboxPrice = await ReboxPrice.findOrFail(reboxPrices.id);
        city = reboxPrice.city
        state = reboxPrice.state
        var { price, km, per_km, commission } = reboxPrices;
        reboxPrice.merge({ price, km, per_km, commission });
        await reboxPrice.save();
        results.push(reboxPrice);
      }
    }
    if (extras[0].cost !== undefined) {
      for (const extra of extras) {
        var serviceCost = await ServiceCost.findOrFail(extra.id)
        serviceCost.cost = extra.cost
        await serviceCost.save()
        results.push(serviceCost)
      }
    } else {
      for (const extra of extras) {
        var reboxExtra = await StatusVehicle.query().whereRaw(`UPPER(description) = '${extra.description.toUpperCase()}'`).where('city', city).where('state', state).first()
        if (!reboxExtra)
          reboxExtra = await RemovalLocation.query().whereRaw(`UPPER(description) = '${extra.description.toUpperCase()}'`).where('city', city).where('state', state).first()
        reboxExtra.price = extra.price
        await reboxExtra.save()
        results.push(reboxExtra);
      }
    }


    return results;
  }
}

module.exports = PricesController;
