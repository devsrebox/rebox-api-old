'use strict'

const { USER_ROLE } = require("../../../../Utils/Constans")

const Partner = use('App/Models/Partner')
const Rebox = use('App/Models/ReboxService')
const User = use('App/Models/User')
const PartnersType = use('App/Models/PartnersType')
const ServiceLocation = use('App/Models/ServiceLocation')


class PartnersController {

  async index({ pagination, request }) {
    const { search, order } = request.get()
    const results = await Partner.query()
      .select('partners.*')
      .select('partners.id as partner_id')
      .select('users.*')
      .join('users', 'users.id', 'partners.user_id')
      .where(function() {
        if (search)
          this.where('users.name', 'LIKE', `%${search}%`)
          this.orWhere('partners.name', 'LIKE', `%${search}%`)
          this.orWhere('partners.cnpj', 'LIKE', `%${search}%`)
          this.orWhere('partners.company_name', 'LIKE', `%${search}%`)
      })
      .orderBy(order || 'users.name')
      .paginate(pagination.page, pagination.perpage)

    return results
  }

  async show({ params }) {
    const partner = await Partner.findOrFail(params.id)

    await partner.load('user')
    await partner.load('bank_account')
    await partner.load('types')
    await partner.load('serviceLocations')
    return partner
  }

  async store({ request, response }) {
    const partner = await Partner.create({})

    const { bank_account, contact1, contact2, number_trucks, cod, cnpj,
      company_name, name, ie, im, cep, address, city, uf, neighborhood,
      contract_of, contract_until, types, user, serviceLocations } = request.post()
    const userExist = await User.findBy('email', user.email || 'te@te')
    if (userExist) {
      return response.status(409).send({
        message: 'Email já cadastrado'
      })
    }
    partner.merge(
      {
        contact1, contact2, number_trucks, cod, cnpj,
        company_name, name, ie, im, cep, address, city, uf, neighborhood, contract_of, contract_until, status: true
      }
    )

    if (bank_account) {
      await partner.bank_account().delete()
      await partner.bank_account().create(bank_account)
    }

    if (serviceLocations) {
      await partner.serviceLocations().delete()
      await partner.serviceLocations().createMany(serviceLocations)
    }

    await partner.save()

    if (types) {
      await partner.types().detach()
      await partner.types().attach(types)
    }
    const userSaved = await User.create({...user, role: USER_ROLE.PARTNER})
    await partner.user().associate(userSaved)

    await partner.save()
    await partner.load('user')
    await partner.load('types')
    await partner.load('bank_account')

    return response.status(200).send({
      message: 'dados salvos com sucesso',
      partner
    })

  }

  async update({ params, response, request }) {
    const partner = await Partner.findOrFail(params.id)
    const findUser = await partner.user().fetch

    const { bank_account, contact1, contact2, number_trucks, cod, cnpj,
      company_name, name, ie, im, cep, address, city, uf, neighborhood, contract_of, contract_until, types, user } = request.post()

    partner.merge(
      {
        contact1, contact2, number_trucks, cod, cnpj,
        company_name, name, ie, im, cep, address, city, uf, neighborhood, contract_of, contract_until
      }
    )

    if (bank_account) {
      await partner.bank_account().delete()
      await partner.bank_account().create(bank_account)
    }

    await partner.save()

    if (types) {
      await partner.types().detach()
      await partner.types().attach(types)
    }

    if (user) {
      const userExist = await User.findBy('email', findUser.email || 'te@te')
      if (userExist && user.email != userExist.email) {
        return response.status(409).send({
          message: 'Email já cadastrado, dados parcialmente salvos!'
        })
      }

      const userSaved = await partner.user().fetch()
      userSaved.merge(user)
      await userSaved.save()
    }

    await partner.save()
    await partner.load('user')
    await partner.load('types')
    await partner.load('bank_account')

    return response.status(200).send({
      message: 'dados salvos com sucesso',
      partner
    })

  }

  async destroy({ params }) {
    const partner = await Partner.findOrFail(params.id)

    const user = await partner.user().fetch()

    const reboxs = await Rebox.query().where('partner_id', partner.id).fetch()

    for (const rebox of reboxs.rows) {
      rebox.partner_id = null
      await rebox.save()
    }

    await PartnersType.query().where('partner_id', partner.id).delete()

    await partner.delete()
    await user.tokens().delete()
    await user.delete()

  }

  async statesAndCities() {
    const results  = await ServiceLocation.query()
    .distinct('state')
    .distinct('city')
    .fetch()

    return results
  }

  async partnersByStatesAndCities({params}) {
    const results = await Partner.query()
    .whereHas('serviceLocations', (builder) => {
      builder.whereRaw(`UPPER(state) like '%${params.state.toUpperCase()}%'` )
      builder.whereRaw(`UPPER(city) like '%${decodeURIComponent(params.city).toUpperCase()}%'` )

    })
    .fetch()

    return results
  }

}

module.exports = PartnersController
