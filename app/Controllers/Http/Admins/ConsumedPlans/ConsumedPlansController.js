"use strict";

const ConsumedPlan = use("App/Models/ConsumedPlan");
const Contract = use("App/Models/Contract");
const User = use("App/Models/User");
const UsersPlan = use("App/Models/UsersPlan");
const Partner = use("App/Models/Partner");
const moment = use("moment");
const Ws = use("Ws");
class ConsumedPlansController {
  async index({ pagination }) {
    const results = await ConsumedPlan.query()
      .with("use_plan", (builder) => {
        builder.with("user");
      })
      .orderBy("created_at", "desc")
      .paginate(pagination.page, pagination.perpage);

    return results;
  }

  async show({ params }) {
    const consumedPlan = await ConsumedPlan.query()
      .where("id", params.id)
      .with("use_plan", (builder) => {
        builder.with("user");
      })
      .with("partner")
      .first();

    return consumedPlan;
  }

  async partners({ request }) {
    const { lat1, lng1} = await request.get();
    const GoogleService = use("App/Services/GoogleService");
    const objGoogleService = new GoogleService();
    const data = await objGoogleService.getAddress(lat1, lng1);
    const respData = JSON.parse(data);

    const resp = respData.results.find((data) => {
      return data.types.find((address) => {
        return address.includes("administrative_area_level_2");
      });
    });

    const city = resp.address_components[0].long_name;
    const state = resp.address_components[1].short_name;

    var partnes = [];
    partnes = await Partner.query()
      .where("status", true)
      .whereHas("serviceLocations", (builder) => {
        builder.where("state", state);
        builder.where("city", city);
      })
      .fetch();

    if (!partnes) {
      partnes = await Partner.query()
        .where("status", true)
        .whereHas("serviceLocations", (builder) => {
          builder.where("state", state);
        })
        .fetch();
    }
    if (!partnes) {
      partnes = await Partner.query().where("status", true).fetch();
    }
    return partnes;
  }

  async update({ params, request }) {
    const consumedPlan = await ConsumedPlan.findOrFail(params.id);
    const { status, partner_id } = request.post();

    if (partner_id) {
      consumedPlan.partner_id = partner_id;
      consumedPlan.status = "repassado";
      await consumedPlan.save();
    }
    if (status) consumedPlan.status = status;
    await consumedPlan.save();

    if (status && status == "reprovado") {
      const user_plan = await consumedPlan.use_plan().fetch();
      user_plan.is_active = true;
      user_plan.uses += 1;
      await user_plan.save();
      const servicesActives = await UsersPlan.query()
        .where("is_active", false)
        .where("user_id", user_plan.user_id)
        .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
        .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
        .fetch();

      for (const row of servicesActives.rows) {
        row.is_active = true;
        await row.save();
      }
    }

    const topic = Ws.getChannel("notifications").topic("notifications");
    if (topic) {
      topic.broadcast("new:service", {});
    }
  }

}

module.exports = ConsumedPlansController;
