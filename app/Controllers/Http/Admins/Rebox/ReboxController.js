'use strict'

const Rebox = use('App/Models/ReboxService')

class ReboxController {

    async index({pagination}) {
      const results = await Rebox.query()
      .paginate(pagination.page, pagination.perpage)

      return results
    }

    async destroy({params}) {
      const rebox = await Rebox.findOrFail(params.id)

      await rebox.assessments().delete()

      await rebox.payments().delete()

      await rebox.reject_partner().detach()

      await rebox.extracts().delete()


      await rebox.delete()
    }

}

module.exports = ReboxController
