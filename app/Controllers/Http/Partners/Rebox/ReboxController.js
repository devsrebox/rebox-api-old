'use strict'

const { consumedPlans } = require("../../../../Services/ConsumedPlans")
const { LOGO_URL, MAX_CONSUMED_PLANS } = require("../../../../Utils/Constans")

const Rebox = use('App/Models/ReboxService')
const Partner = use('App/Models/Partner')

const HistoryRequest = use('App/Models/HistoryRequest')
const StatusVehicle = use('App/Models/StatusVehicle')
const RemovalLocation = use('App/Models/RemovalLocation')
const CONSTANTS = use('App/Utils/Constans')
const Ws = use('Ws')
const Mail = use('Mail')
const Env = use('Env')
const moment = use('moment')
class ReboxController {

  async finishStatus({ params }) {
    const rebox = await Rebox.findOrFail(params.reboxid)
    rebox.status = CONSTANTS.STATUS_SERVICE.FINALIZADO
    const topic = Ws.getChannel('notifications').topic('notifications')
    if (topic) {
      topic.broadcast('change:rebox', rebox)
    }
    await rebox.save()
  }

  async rejectRebox({ params }) {
    const rebox = await Rebox.findOrFail(params.reboxid)

    await rebox.reject_partner().attach(params.partnerid)

    const topic = Ws.getChannel('notifications').topic('notifications')
    if (topic) {
      topic.broadcast('change:rebox', rebox)
    }
  }


  async confirmStatus({ params, response, request }) {
    const partner = await Partner.findOrFail(params.partnerid)
    const rebox = await Rebox.findOrFail(params.reboxid)
    if (rebox.partner_id) {
      return response.status(401).send({
        message: 'Serviço já confirmado por outro parceiro'
      })
    }
    const { forecast, name_partner, attendant_id } = request.post()
    rebox.partner_id = partner.id
    rebox.forecast = forecast.substr(0, 2) + ":" + forecast.substr(2, 2) + ':00' || '20:00'
    rebox.name_partner = name_partner || ''
    rebox.status = CONSTANTS.STATUS_SERVICE.ANDAMENTO
    rebox.attendant_id = attendant_id
    await rebox.save()
    await rebox.load('partner')
    const topic = Ws.getChannel('notifications').topic('notifications')
    if (topic) {
      topic.broadcast('change:rebox', rebox)
    }

    const vehicleUser = await rebox.vehicle().fetch()
    const userRebox = await vehicleUser.user().fetch()
    const partnerUser = await partner.user().fetch()
    const status = await StatusVehicle.find(rebox.status_vehicle_id)
    const remove = await RemovalLocation.find(rebox.removal_location_id)
    const date = moment(rebox.created_at).format('DD/MM/YYYY')
    const hour = moment(rebox.created_at).format('HH:mm')

    await HistoryRequest.create({
      partner_id: partner.id,
      user_id: userRebox.id,
      from: rebox.from,
      to: rebox.to,
      audity_id: rebox.id,
      description: 'Reboque',
      price: rebox.amount,
      type: 'avulso'
    })

    const isTest = Env.get('NODE_ENV')
    if (isTest == 'test') {
      return response.status(200).send({
        message: 'Serviço confirmado'
      })
    }


    await Mail.send('notifier_rebox', { rebox, user: userRebox, vehicle: vehicleUser, status, remove, date, hour, logo: LOGO_URL }, (message) => {
      message
        .to(userRebox.email)
        .from('noreplay@rebox.com.br')
        .subject('Novo reboque acionado')
    })
    await Mail.send('notifier_rebox_company', { rebox }, (message) => {
      message
        .to(partnerUser.email)
        .from('noreplay@rebox.com.br')
        .subject('Serviço confirmado')
    })
    return response.status(200).send({
      message: 'Serviço confirmado'
    })
  }

}

module.exports = ReboxController
