'use strict'
const path = require('path')
const Partner = use('App/Models/Partner')
const Excel = use('exceljs')
const Database = use('Database')
const moment = use('moment')
//Required package
const pdf = require("pdf-creator-node");
const fs = require('fs');
const { MaskCNPJ } = require('../../../../Utils/Mask')
const { STATUS_PAYMENT } = require('../../../../Utils/Constans')
const Helpers = use('Helpers')
const Mail = use('Mail')
const Env = use('Env')

const finalStatusServiceRebox = 'finalizado'

class ReportsController {

  async finance({ params }) {
    const results = await Partner.query()
      .with('user')
      .where('id', params.partnerid)
      .with('requests', (builder) => {
        builder.with('extracts')
      }).first()
    return results
  }

  async operationalByDate(initial_date, final_date, partnerID) {

    const results = await Partner.query()
      .select('partners.*')
      .select('rebox_services.*')
      .select('users.*')
      .select('vehicles.*')
      .select('rebox_services.id as service_id')
      .join('rebox_services', 'rebox_services.partner_id', '=', 'partners.id').as('service')
      .join('vehicles', 'rebox_services.vehicle_id', 'vehicles.id')
      .join('users', 'users.id', '=', 'vehicles.user_id')
      .where('partners.id', partnerID)
      .where('rebox_services.created_at', '>=', initial_date)
      .where('rebox_services.created_at', '<=', final_date)
      // .where('rebox_services.status', '=', finalStatusServiceRebox)

      .fetch()
    const resultFormated = results.rows.map((value) => {
      var obj = value.$attributes
      obj.amount = this.numberToReal(obj.amount);
      obj.count = results.rows.length
      return obj
    })
    return resultFormated
  }

  async operationalByDateToExcel({ response, request }) {
    const { initial_date, final_date, email, partner_id=0 } = request.get()
    if (!initial_date || !final_date) {
      return response.status(400).send({
        message: 'Data inicial e final são obrigatórias'
      })
    }
    const results = await this.mainOperational(initial_date, final_date, partner_id)


    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet("Operacional");
    this.addHeaderOperationalExcel(worksheet)

    var idParner = 0
    var contLine = 1
    for (const result of results) {

      if (idParner != result.id) {

        worksheet.addRow({}) // + 1 linha
        contLine++

        const row = worksheet.addRow({
          a: 'Prestador:',
          b: result.company_name,
          c: '',
          d: 'CNPJ',
          e: MaskCNPJ(result.cnpj),
          f: '',
          g: 'Período',
          h: moment(initial_date).format('DD/MM/YYYY'),
          i: moment(final_date).format('DD/MM/YYYY')
        });
        contLine++
        row.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: '07359b' },
          bgColor: { argb: 'ffff00' }
        };
        row.font = { color: { argb: 'ffffff' } }
        worksheet.addRow({}) // + 1 linha
        contLine++
        worksheet.mergeCells(`A${contLine}:B${contLine}`);
        worksheet.mergeCells(`C${contLine}:D${contLine}`);
        worksheet.mergeCells(`F${contLine}:G${contLine}`);
        worksheet.mergeCells(`H${contLine}:I${contLine}`);
        worksheet.mergeCells(`J${contLine}:L${contLine}`);
        worksheet.getRow(contLine).getCell('A').value = 'KM Percorrido';
        worksheet.getRow(contLine).getCell('A').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('C').value = 'Atendimentos';
        worksheet.getRow(contLine).getCell('C').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('F').value = 'Total';
        worksheet.getRow(contLine).getCell('F').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('H').value = 'Para Rebox';
        worksheet.getRow(contLine).getCell('H').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('J').value = 'Para Parceiro ';
        worksheet.getRow(contLine).getCell('J').alignment = { horizontal: 'center' };// + 1 linha
        contLine++
        worksheet.mergeCells(`A${contLine}:B${contLine}`);
        worksheet.mergeCells(`C${contLine}:D${contLine}`);
        worksheet.mergeCells(`F${contLine}:G${contLine}`);
        worksheet.mergeCells(`H${contLine}:I${contLine}`);
        worksheet.mergeCells(`J${contLine}:L${contLine}`);
        worksheet.getRow(contLine).getCell('A').value = result.kmSum;
        worksheet.getRow(contLine).getCell('A').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('C').value = result.count;
        worksheet.getRow(contLine).getCell('C').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('F').value = result.total;
        worksheet.getRow(contLine).getCell('F').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('H').value = result.rebox;
        worksheet.getRow(contLine).getCell('H').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('J').value = result.partner;
        worksheet.getRow(contLine).getCell('J').alignment = { horizontal: 'center' };

        const hrow = worksheet.addRow({
          a: 'ID',
          b: 'Nome',
          c: 'KM',
          d: `Porte/Marca`,
          e: 'Origem',
          f: 'Destino',
          g: 'Total',
          h: 'Motorista',
          i: 'Data e hora'
        });
        contLine++
        hrow.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: 'D3D3D3' },
          bgColor: { argb: 'ffffff' }
        };

      }
      worksheet.addRow({
        a: result.service_id,
        b: result.nameuser,
        c: result.distance,
        d: `${result.port} / ${result.brand}`,
        e: result.from,
        f: result.to,
        g: result.amount,
        h: result.name_partner,
        i: moment(result.created_at).format('DD/MM/YYYY HH:mm')
      });
      idParner = result.id
      contLine++
    }
    // save under export.xlsx
    await workbook.xlsx.writeFile('export.xlsx');

    if (email) {
      await this.sendReportsForEmail('export.xlsx', email)
    }

    return response.download('export.xlsx');


  };

  async operationalDetailsToPdf({ response, request, params }) {
    const { initial_date, final_date, email } = request.get()
    if (!initial_date || !final_date) {
      return response.status(400).send({
        message: 'Data inicial e final são obrigatórias'
      })
    }

    const results = await this.mainOperational(initial_date, final_date, params.partnerid)
    var html = fs.readFileSync(Helpers.resourcesPath('templates/operational_to_pdf.html'), 'utf8');
    var options = {
      format: "A4",
      orientation: "portrait",
      border: "10mm",
      footer: {
        height: "28mm",
        contents: {
          first: 'Primeira página',
          2: 'Segunda página', // Any page number is working. 1-based index
          default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', // fallback value
          last: 'ùltima página'
        }
      }
    }
    var document = {
      html: html,
      data: {
        initial_date: moment(initial_date).format('DD/MM/yyyy'),
        final_date: moment(final_date).format('DD/MM/yyyy'),
        results,
      },
      path: Helpers.tmpPath('prestadores_operacional.pdf')

    };
    await pdf.create(document, options)

    if (email) {
      await this.sendReportsForEmail(Helpers.tmpPath('prestadores_operacional.pdf'), email)
    }

    return response.download(Helpers.tmpPath('prestadores_operacional.pdf'));

  }

  async sendReportsForEmail(pathFile, email) {
    const fileNmae = path.basename(pathFile);
    await Mail.raw(`<h1> Segue anexado o relatório expedido em ${moment().subtract(3, 'hours').format('DD/MM/YYYY HH:ss')} </h1>`, (message) => {
      message.from(Env.get('MAIL_USERNAME'))
      message.to(email)
      message.subject('Relatórios - Rebox')
      message
        .attach(pathFile, {
          filename: fileNmae
        })
    })

  }

  async financeInPdf({ response, request, auth }) {
    const user = await auth.getUser()
    const partner = await user.partner().fetch()
    const { initial_date, final_date, email} = request.get()
    if (!initial_date || !final_date) {
      return response.status(400).send({
        message: 'Data inicial e final são obrigatórias'
      })
    }

    const results = await this.mainFinance(initial_date, final_date, partner.id)


    var html = fs.readFileSync(Helpers.resourcesPath('templates/finance_to_pdf.html'), 'utf8');
    var options = {
      format: "A4",
      orientation: "portrait",
      border: "10mm",
      footer: {
        height: "28mm",
        contents: {
          first: 'Primeira página',
          2: 'Segunda página', // Any page number is working. 1-based index
          default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', // fallback value
          last: 'ùltima página'
        }
      }
    }
    var document = {
      html: html,
      data: {
        initial_date: moment(initial_date).format('DD/MM/yyyy'),
        final_date: moment(final_date).format('DD/MM/yyyy'),
        results
      },
      path: Helpers.tmpPath('prestadores.pdf')

    };
    await pdf.create(document, options)


    if (email) {
      await this.sendReportsForEmail(Helpers.tmpPath('prestadores.pdf'), user.email)
    }

    return response.download(Helpers.tmpPath('prestadores.pdf'));
  }

  numberToReal(numero) {
    var numero = numero.toFixed(2).split('.');
    numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
    return numero.join(',');
  }

  async mainFinance(initial_date, final_date, partnerID = 0) {
    const newResults = await Partner.query()
      .select(Database.raw(`SUM(rebox_extracts.value) as totalService,
       rebox_services.id as service_id,
       rebox_services.created_at as created,
       users.name as nameuser,
       rebox_services.*,
       vehicles.*,
       port_cars.*,
       users.*,
       partners.*,
       status_payments.*,
       partners.id as partnerid,
      SUM(rebox_extracts.partner_value) as partnerservice,
      SUM(rebox_extracts.rebox_value) as reboxservice`))
      .join('rebox_services', 'rebox_services.partner_id', 'partners.id')
      .join('vehicles', 'rebox_services.vehicle_id', 'vehicles.id')
      .join('users', 'users.id', '=', 'vehicles.user_id')
      .join('port_cars', 'rebox_services.port_car_id', '=', 'port_cars.id')
      .join('status_payments', 'status_payments.rebox_service_id', '=', 'rebox_services.id')
      .join('rebox_extracts', 'rebox_extracts.rebox_service_id', 'rebox_services.id')
      .groupByRaw('rebox_services.id, users.name, vehicles.id, port_cars.id, users.id, partners.id, status_payments.id')
      .where('status_payments.description', '=', STATUS_PAYMENT.PAGO)
      .where(function () {
        if (partnerID != 0) {
          this.where('partners.id', partnerID)
        }
      })
      .where('rebox_services.created_at', '>=', initial_date)
      .where('rebox_services.created_at', '<=', final_date)
      // .where('rebox_services.status', '=', finalStatusServiceRebox)
      .orderBy('partners.id')
      .orderBy('rebox_services.created_at')
      .fetch()

    var rowsFormated = []
    var idParner = 0
    for (const row of newResults.rows) {
      var formatRow = row.$attributes
      formatRow.newHeader = false // header para o relatório
      if (idParner != row.partnerid) {
        idParner = row.partnerid
        formatRow.newHeader = true // header para o relatório

        var resumeDetails = await Database.from('rebox_extracts')
          .select('rebox_extracts.description')
          .select('commission_percent')
          .count('*')
          .sum('rebox_extracts.value as total')
          .sum('rebox_extracts.rebox_value as rebox')
          .sum('rebox_extracts.partner_value as partner')
          .join('rebox_services', 'rebox_services.id', 'rebox_extracts.rebox_service_id')
          .join('vehicles', 'rebox_services.vehicle_id', 'vehicles.id')
          .join('users', 'users.id', '=', 'vehicles.user_id')
          .join('port_cars', 'rebox_services.port_car_id', '=', 'port_cars.id')
          .join('partners', 'partners.id', 'rebox_services.partner_id')
          .join('status_payments', 'status_payments.rebox_service_id', '=', 'rebox_services.id')
          .where('status_payments.description', '=', STATUS_PAYMENT.PAGO)
          .where('partners.id', row.partnerid)
          .where('rebox_services.created_at', '>=', initial_date)
          .where('rebox_services.created_at', '<=', final_date)
          // .where('rebox_services.status', '=', finalStatusServiceRebox)
          .groupBy('rebox_extracts.description')
          .groupBy('rebox_extracts.commission_percent')

        var kmSum = await Database.from('rebox_services')
          .sum(`distance as km`)
          .where('partner_id', row.partnerid)
          .where('rebox_services.created_at', '>=', initial_date)
          .where('rebox_services.created_at', '<=', final_date)

        // .where('rebox_services.status', '=', finalStatusServiceRebox)


        var count = await Database.from('rebox_services')
          .count(`distance`)
          .where('partner_id', row.partnerid)
          .where('rebox_services.created_at', '>=', initial_date)
          .where('rebox_services.created_at', '<=', final_date)

        // .where('rebox_services.status', '=', finalStatusServiceRebox)

        var extract = await Database.from('rebox_services')
          .join('rebox_extracts', 'rebox_extracts.rebox_service_id', 'rebox_services.id')
          .sum(`rebox_extracts.value as amount`)
          .sum(`rebox_extracts.rebox_value as rebox`)
          .sum(`rebox_extracts.partner_value as partner`)
          .where('partner_id', row.partnerid)
          .where('rebox_services.created_at', '>=', initial_date)
          .where('rebox_services.created_at', '<=', final_date)

        formatRow.kmSum = kmSum[0].km.toFixed(2)
        formatRow.count = count[0].count
        formatRow.total = this.numberToReal(extract[0].amount)
        formatRow.rebox = this.numberToReal(extract[0].rebox)
        formatRow.partner = this.numberToReal(extract[0].partner)
        formatRow.totalDetails = resumeDetails.map((resume) => {
          resume.total = this.numberToReal(resume.total)
          resume.rebox = this.numberToReal(resume.rebox)
          resume.partner = this.numberToReal(resume.partner)
          return resume
        })

      }
      formatRow.reboxservice = this.numberToReal(formatRow.reboxservice)
      formatRow.partnerservice = this.numberToReal(formatRow.partnerservice)
      formatRow.amount = this.numberToReal(formatRow.amount)
      formatRow.cnpj = MaskCNPJ(formatRow.cnpj)
      const data = new Date(row.created)
      formatRow.created_at = moment()
        .date(data.getDate())
        .month(data.getMonth())
        .year(data.getFullYear())
        .hour(parseInt(data.getHours()))
        .minute(parseInt(data.getMinutes()))
        .format('DD/MM/YYYY HH:mm')
      rowsFormated.push(formatRow)
    }
    return rowsFormated

  }

  async operationalInPdf({ request, response, auth }) {
    const user = await auth.getUser()
    const partner = await user.partner().fetch()
    const { initial_date, final_date, email } = request.get()
    if (!initial_date || !final_date) {
      return response.status(400).send({
        message: 'Data inicial e final são obrigatórias'
      })
    }

    const results = await this.mainOperational(initial_date, final_date, partner.id)

    var html = fs.readFileSync(Helpers.resourcesPath('templates/operational_to_pdf.html'), 'utf8');
    var options = {
      format: "A4",
      orientation: "portrait",
      border: "10mm",
      footer: {
        height: "28mm",
        contents: {
          first: 'Primeira página',
          2: 'Segunda página', // Any page number is working. 1-based index
          default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', // fallback value
          last: 'ùltima página'
        }
      }
    }
    var document = {
      html: html,
      data: {
        initial_date: moment(initial_date).format('DD/MM/yyyy'),
        final_date: moment(final_date).format('DD/MM/yyyy'),
        results,
      },
      path: Helpers.tmpPath('prestadores_operacional.pdf')

    };
    await pdf.create(document, options)

    if (email) {
      await this.sendReportsForEmail(Helpers.tmpPath('prestadores_operacional.pdf'), user.email)
    }

    return response.download(Helpers.tmpPath('prestadores_operacional.pdf'));

  }




  async mainOperational(initial_date, final_date, partnerID = 0) {

    const newResults = await Partner.query()
      .select('rebox_services.id as service_id')
      .select('rebox_services.created_at as created')
      .select('users.name as nameuser')
      .select('rebox_services.*')
      .select('vehicles.*')
      .select('port_cars.*')
      .select('users.*')
      .select('partners.*')
      .join('rebox_services', 'rebox_services.partner_id', 'partners.id')
      .join('vehicles', 'rebox_services.vehicle_id', 'vehicles.id')
      .join('users', 'users.id', '=', 'vehicles.user_id')
      .join('port_cars', 'rebox_services.port_car_id', '=', 'port_cars.id')
      .where(function () {
        if (partnerID != 0) {
          this.where('partners.id', partnerID)
        }
      })
      .where('rebox_services.created_at', '>=', initial_date)
      .where('rebox_services.created_at', '<=', final_date)
      // .where('rebox_services.status', '=', finalStatusServiceRebox)
      .orderBy('partners.id')
      .orderBy('rebox_services.created_at')
      .fetch()
    var rowsFormated = []
    var idParner = 0
    for (const row of newResults.rows) {
      var formatRow = row.$attributes
      formatRow.newHeader = false // header para o relatório
      if (idParner != row.id) {
        idParner = row.id
        formatRow.newHeader = true // header para o relatório

        var kmSum = await Database.from('rebox_services')
          .sum(`distance as km`)
          .where('partner_id', row.id)
          .where('rebox_services.created_at', '>=', initial_date)
          .where('rebox_services.created_at', '<=', final_date)

        // .where('rebox_services.status', '=', finalStatusServiceRebox)


        var count = await Database.from('rebox_services')
          .count(`distance`)
          .where('partner_id', row.id)
          .where('rebox_services.created_at', '>=', initial_date)
          .where('rebox_services.created_at', '<=', final_date)

        // .where('rebox_services.status', '=', finalStatusServiceRebox)

        var extract = await Database.from('rebox_services')
          .join('rebox_extracts', 'rebox_extracts.rebox_service_id', 'rebox_services.id')
          .sum(`rebox_extracts.value as amount`)
          .sum(`rebox_extracts.rebox_value as rebox`)
          .sum(`rebox_extracts.partner_value as partner`)
          .where('partner_id', row.id)
          .where('rebox_services.created_at', '>=', initial_date)
          .where('rebox_services.created_at', '<=', final_date)

        formatRow.kmSum = kmSum[0].km.toFixed(2)
        formatRow.count = count[0].count
        formatRow.total = this.numberToReal(extract[0].amount)
        formatRow.rebox = this.numberToReal(extract[0].rebox)
        formatRow.partner = this.numberToReal(extract[0].partner)


      }
      formatRow.amount = this.numberToReal(formatRow.amount)
      formatRow.cnpj = MaskCNPJ(formatRow.cnpj)
      const data = new Date(row.created)
      formatRow.created_at = moment()
        .date(data.getDate())
        .month(data.getMonth())
        .year(data.getFullYear())
        .hour(parseInt(data.getHours()))
        .minute(parseInt(data.getMinutes()))
        .format('DD/MM/YYYY HH:mm')
      rowsFormated.push(formatRow)
    }
    return rowsFormated

  }

  async financeExecel({ response, request, auth }) {
    const user = await auth.getUser()
    const partner = user.partner().fetch()
    const { initial_date, final_date, email } = request.get()
    if (!initial_date || !final_date) {
      return response.status(400).send({
        message: 'Data inicial e final são obrigatórias'
      })
    }

    const results = await this.mainFinance(initial_date, final_date, partner.id)


    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet("Financeiro");
    this.addHeaderOperationalExcel(worksheet)

    var idParner = 0
    var contLine = 1
    for (const result of results) {

      if (idParner != result.partnerid) {

        worksheet.addRow({}) // + 1 linha
        contLine++

        const row = worksheet.addRow({
          a: 'Prestador:',
          b: result.company_name,
          c: '',
          d: 'CNPJ',
          e: MaskCNPJ(result.cnpj),
          f: '',
          g: 'Período',
          h: moment(initial_date).format('DD/MM/YYYY'),
          i: moment(final_date).format('DD/MM/YYYY')
        });
        contLine++
        row.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: '07359b' },
          bgColor: { argb: 'ffff00' }
        };
        row.font = { color: { argb: 'ffffff' } }
        worksheet.addRow({}) // + 1 linha
        contLine++

        if (Array.isArray(result.totalDetails)) {
          const sumHeader = worksheet.addRow({
            a: 'Descrição	',
            c: 'Qtd',
            d: 'Porcentagem',
            e: `Total`,
            f: 'Rebox',
            g: 'Prestador',
            h: ''
          }) // + 1 linha
          contLine++

          sumHeader.fill = {
            type: 'pattern',
            pattern: 'solid',
            fgColor: { argb: 'D3D3D3' },
            bgColor: { argb: 'ffffff' }
          };
          worksheet.mergeCells(`A${contLine}:B${contLine}`);
          for (const details of result.totalDetails) {
            worksheet.addRow({
              a: details.description,
              c: details.count,
              d: details.commission_percent,
              e: details.total,
              f: details.rebox,
              g: details.partner,
              h: '',
              i: '',

            }) // + 1 linha
            contLine++
            worksheet.mergeCells(`A${contLine}:B${contLine}`);
          }
        }
        worksheet.addRow({}) // + 1 linha
        contLine++
        worksheet.mergeCells(`A${contLine}:B${contLine}`);
        worksheet.mergeCells(`C${contLine}:D${contLine}`);
        worksheet.mergeCells(`F${contLine}:G${contLine}`);
        worksheet.mergeCells(`H${contLine}:I${contLine}`);
        worksheet.mergeCells(`J${contLine}:L${contLine}`);
        worksheet.getRow(contLine).getCell('A').value = 'KM Percorrido';
        worksheet.getRow(contLine).getCell('A').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('C').value = 'Atendimentos';
        worksheet.getRow(contLine).getCell('C').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('F').value = 'Total';
        worksheet.getRow(contLine).getCell('F').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('H').value = 'Para Rebox';
        worksheet.getRow(contLine).getCell('H').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('J').value = 'Para Parceiro ';
        worksheet.getRow(contLine).getCell('J').alignment = { horizontal: 'center' };// + 1 linha
        contLine++
        worksheet.mergeCells(`A${contLine}:B${contLine}`);
        worksheet.mergeCells(`C${contLine}:D${contLine}`);
        worksheet.mergeCells(`F${contLine}:G${contLine}`);
        worksheet.mergeCells(`H${contLine}:I${contLine}`);
        worksheet.mergeCells(`J${contLine}:L${contLine}`);
        worksheet.getRow(contLine).getCell('A').value = result.kmSum;
        worksheet.getRow(contLine).getCell('A').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('C').value = result.count;
        worksheet.getRow(contLine).getCell('C').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('F').value = result.total;
        worksheet.getRow(contLine).getCell('F').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('H').value = result.rebox;
        worksheet.getRow(contLine).getCell('H').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('J').value = result.partner;
        worksheet.getRow(contLine).getCell('J').alignment = { horizontal: 'center' };

        const hrow = worksheet.addRow({
          a: 'ID',
          b: 'Nome',
          c: 'KM',
          d: `Porte/Marca`,
          e: 'Tipo de pagamento',
          f: 'Valor pago',
          g: 'Valor da rebox',
          h: 'Valor do prestador',
          i: 'Data e hora'
        });
        contLine++
        hrow.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: 'D3D3D3' },
          bgColor: { argb: 'ffffff' }
        };


      }
      worksheet.addRow({
        a: result.service_id,
        b: result.nameuser,
        c: result.distance,
        d: `${result.port} / ${result.brand}`,
        e: result.payment_method,
        f: result.amount,
        g: result.reboxservice,
        h: result.partnerservice,
        i: moment(result.created_at).format('DD/MM/YYYY HH:mm')
      });
      idParner = result.partnerid
      contLine++
    }
    // save under export.xlsx
    await workbook.xlsx.writeFile('export.xlsx');

    if (email) {
      await this.sendReportsForEmail('export.xlsx', user.email)
    }

    return response.download('export.xlsx');

  };
  addHeaderOperationalExcel(worksheet) {
    worksheet.columns = [
      { header: '', key: 'a', width: 10 },
      { header: '', key: 'b', width: 32 },
      { header: '', key: 'c', width: 10, },
      { header: '', key: 'd', width: 30, },
      { header: '', key: 'e', width: 20, },
      { header: '', key: 'f', width: 15, },
      { header: '', key: 'g', width: 10, },
      { header: '', key: 'h', width: 20, },
      { header: '', key: 'i', width: 20, },

    ];
    worksheet.mergeCells('A1:C1');
    worksheet.mergeCells('D1:I1');
    worksheet.getRow(1).getCell('A').value = 'REBOX SOLUÇÕES INTEGRADAS';
    worksheet.getRow(1).getCell('A').alignment = { horizontal: 'center' };
    worksheet.getRow(1).getCell('D').value = 'RELATÓRIO OPERACIONAL DE TODOS OS PRESTADORES';
    worksheet.getRow(1).getCell('D').alignment = { horizontal: 'center' };
  }

  async operationalToExecel({ response, request, auth }) {
    const user = await auth.getUser()
    const partner = await user.partner().fetch()

    const { initial_date, final_date, email } = request.get()
    if (!initial_date || !final_date) {
      return response.status(400).send({
        message: 'Data inicial e final são obrigatórias'
      })
    }

    const results = await this.mainOperational(initial_date, final_date, partner.id)


    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet("Operacional");
    this.addHeaderOperationalExcel(worksheet)

    var idParner = 0
    var contLine = 1
    for (const result of results) {

      if (idParner != result.id) {

        worksheet.addRow({}) // + 1 linha
        contLine++

        const row = worksheet.addRow({
          a: 'Prestador:',
          b: result.company_name,
          c: '',
          d: 'CNPJ',
          e: MaskCNPJ(result.cnpj),
          f: '',
          g: 'Período',
          h: moment(initial_date).format('DD/MM/YYYY'),
          i: moment(final_date).format('DD/MM/YYYY')
        });
        contLine++
        row.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: '07359b' },
          bgColor: { argb: 'ffff00' }
        };
        row.font = { color: { argb: 'ffffff' } }
        worksheet.addRow({}) // + 1 linha
        contLine++
        worksheet.mergeCells(`A${contLine}:B${contLine}`);
        worksheet.mergeCells(`C${contLine}:D${contLine}`);
        worksheet.mergeCells(`F${contLine}:G${contLine}`);
        worksheet.mergeCells(`H${contLine}:I${contLine}`);
        worksheet.mergeCells(`J${contLine}:L${contLine}`);
        worksheet.getRow(contLine).getCell('A').value = 'KM Percorrido';
        worksheet.getRow(contLine).getCell('A').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('C').value = 'Atendimentos';
        worksheet.getRow(contLine).getCell('C').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('F').value = 'Total';
        worksheet.getRow(contLine).getCell('F').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('H').value = 'Para Rebox';
        worksheet.getRow(contLine).getCell('H').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('J').value = 'Para Parceiro ';
        worksheet.getRow(contLine).getCell('J').alignment = { horizontal: 'center' };// + 1 linha
        contLine++
        worksheet.mergeCells(`A${contLine}:B${contLine}`);
        worksheet.mergeCells(`C${contLine}:D${contLine}`);
        worksheet.mergeCells(`F${contLine}:G${contLine}`);
        worksheet.mergeCells(`H${contLine}:I${contLine}`);
        worksheet.mergeCells(`J${contLine}:L${contLine}`);
        worksheet.getRow(contLine).getCell('A').value = result.kmSum;
        worksheet.getRow(contLine).getCell('A').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('C').value = result.count;
        worksheet.getRow(contLine).getCell('C').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('F').value = result.total;
        worksheet.getRow(contLine).getCell('F').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('H').value = result.rebox;
        worksheet.getRow(contLine).getCell('H').alignment = { horizontal: 'center' };
        worksheet.getRow(contLine).getCell('J').value = result.partner;
        worksheet.getRow(contLine).getCell('J').alignment = { horizontal: 'center' };

        const hrow = worksheet.addRow({
          a: 'ID',
          b: 'Nome',
          c: 'KM',
          d: `Porte/Marca`,
          e: 'Origem',
          f: 'Destino',
          g: 'Total',
          h: 'Motorista',
          i: 'Data e hora'
        });
        contLine++
        hrow.fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: 'D3D3D3' },
          bgColor: { argb: 'ffffff' }
        };

      }
      worksheet.addRow({
        a: result.service_id,
        b: result.nameuser,
        c: result.distance,
        d: `${result.port} / ${result.brand}`,
        e: result.from,
        f: result.to,
        g: result.amount,
        h: result.name_partner,
        i: moment(result.created_at).format('DD/MM/YYYY HH:mm')
      });
      idParner = result.id
      contLine++
    }
    // save under export.xlsx
    await workbook.xlsx.writeFile('export.xlsx');

    if (email) {
      await this.sendReportsForEmail('export.xlsx', user.email)
    }

    return response.download('export.xlsx');


  }

}


module.exports = ReportsController
