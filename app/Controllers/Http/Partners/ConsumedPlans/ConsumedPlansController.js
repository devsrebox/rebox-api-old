"use strict";
const ConsumedPlan = use("App/Models/ConsumedPlan");
const HistoryRequest = use("App/Models/HistoryRequest");
const Ws = use('Ws')
class ConsumedPlansController {
  async index({ pagination, auth }) {
    const user = await auth.getUser();
    const partner = await user.partner().fetch();
    const results = await ConsumedPlan.query()
      .with("use_plan", (builder) => {
        builder.with("user");
      })
      .where("partner_id", partner.id)
      .orderBy("created_at", "desc")
      .paginate(pagination.page, pagination.perpage);

    return results;
  }

  async show({ params }) {
    const consumedPlan = await ConsumedPlan.query()
      .with("use_plan", (builder) => {
        builder.with("user");
      })
      .where("id", params.id)
      .first();

    return consumedPlan;
  }

  async update({params, request}) {

    const consumedPlan = await ConsumedPlan.findOrFail(params.id)
    const plan_user = await consumedPlan.use_plan().fetch()
    const { status, partner_id } = request.post()
    if (status == 'confirmado' && consumedPlan.status != 'confirmado') {
      await HistoryRequest.create({
        partner_id,
        user_id: plan_user.user_id,
        from: '',
        to: consumedPlan.address,
        audity_id: consumedPlan.id,
        description: plan_user.description,
        price: plan_user.price_pg,
        type: 'plano'
      })
    }
    consumedPlan.merge(request.post())
    await consumedPlan.save()
    const topic = Ws.getChannel("notifications").topic("notifications");
    if (topic) {
      topic.broadcast("new:service", {});
    }
  }
}

module.exports = ConsumedPlansController;
