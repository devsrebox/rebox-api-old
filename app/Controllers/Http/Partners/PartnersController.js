'use strict'

const Partner = use('App/Models/Partner')

class PartnersController {

  async update({ params, request, response }) {

    const partner = await Partner.findOrFail(params.id)

    const { bank_account, contact1, contact2, number_trucks, cod, cnpj,
       company_name, name, ie, im, cep, address, city, uf, neighborhood,
       contract_of, contract_until, types, serviceLocations} = request.post()

    partner.merge(
      {
        contact1, contact2, number_trucks, cod, cnpj,
        company_name, name, ie, im, cep, address, city, uf, neighborhood, contract_of, contract_until
      }
    )

    if (types) {
      await partner.types().detach()
      await partner.types().attach(types)
    }

    if(bank_account) {
      await partner.bank_account().delete()
      await partner.bank_account().create(bank_account)
    }

    if (serviceLocations) {
      await partner.serviceLocations().delete()
      await partner.serviceLocations().createMany(serviceLocations)
    }


    await partner.save()
    await partner.load('bank_account')
    return response.status(200).send({
      message: 'dados salvos com sucesso'
    })


  }

  async show({params}) {
    const result = await Partner.query()
    .where('id', params.id)
    .with('user')
    .with('types')
    .with('bank_account')
    .with('serviceLocations')
    .first()
    return result
  }

  async confirmStatus({params}) {
    const partner = await Partner.findOrFail(params.partnerid)

    partner.status = true
    await partner.save()

    return 'Liberado o perfil do prestador - ' + partner.cnpj
  }
  async changeStatus({params}) {
    const partner = await Partner.findOrFail(params.partnerid)

    partner.status = false
    await partner.save()

    return 'Bloqueado o perfil do prestador - ' + partner.cnpj
  }


}

module.exports = PartnersController
