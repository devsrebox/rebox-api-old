'use strict'

const Constans = require("../../../../Utils/Constans")

const User = use('App/Models/User')
const Attendant = use('App/Models/Attendant')

class AttendantsController {


  async index({ pagination, auth }) {
    const user = await auth.getUser()
    const partner = await user.partner().fetch()

    const results = await Attendant.query()
      .where('partner_id', partner.id)
      .with('user')
      .paginate(pagination.page, pagination.perpage)

    return results
  }

  async show({params}) {
    const result = await Attendant.query()
    .where('id', params.id)
    .with('user')
    .first()

    return result
  }

  async store({ auth, request, response }) {
    const user = await auth.getUser()
    const partner = await user.partner().fetch()

    const {
      email, cpf, name, cel, tel, password
    } = request.post()
    var userExist = await User.query().where('email', email).first()
    if (userExist) {
      return response.status(409).send({
        message: 'Email já cadastrado!',
        user: userExist
      })
    }
    var userExist = await User.query().where('cpf', cpf).first()
    if (userExist) {
      return response.status(409).send({
        message: 'CPF já cadastrado!',
        user: userExist
      })
    }
    const userSaved = await User.create({ email, cpf, name, cel, tel, password, role: Constans.USER_ROLE.ATTENDANT })

    await Attendant.create({
      partner_id: partner.id,
      user_id: userSaved.id
    })

    return response.status(200).send({
      message: 'Dados atualizados com sucesso!',
      user: userSaved
    })

  }

  async update({ params, request, response }) {

    const attendant = await Attendant.findOrFail(params.id)

    const attendantUser = await attendant.user().fetch()
    const {
      email, cpf, name, cel, tel
    } = request.post()
    var userExist = await User.query().where('email', email).first()
    if (userExist && email != attendantUser.email) {
      return response.status(409).send({
        message: 'Email já cadastrado!',
        user: userExist
      })
    }
    var userExist = await User.query().where('cpf', cpf).first()
    if (userExist && cpf != attendantUser.cpf) {
      return response.status(409).send({
        message: 'Cpf já cadastrado!',
        user: userExist
      })
    }
    attendantUser.merge({ email, cpf, name, cel, tel })
    await attendantUser.save()

    return response.status(200).send({
      message: 'Dados do usuário atualizados',
      user: attendantUser
    })

  }

  async destroy({params, auth, response}) {
    const userLogg = await auth.getUser()

    const partnerLogg = await userLogg.partner().fetch()
    console.log(partnerLogg)
    const attendant = await Attendant.findOrFail(params.id)
    console.log(attendant)
    if (partnerLogg.id != attendant.partner_id) {
      return response.status(401).send({
        message: 'Prestador não corresponde a mesma empresa do atendente'
      })
    }

    const reboxs = await attendant.rebox().fetch()

    for (const rebox of reboxs.rows) {
      rebox.attendant_id = null
      await rebox.save()
    }

    const user = await attendant.user().fetch()

    await user.tokens().delete()

    await user.delete()

    await attendant.delete()

  }

}

module.exports = AttendantsController
