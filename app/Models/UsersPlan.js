'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UsersPlan extends Model {
  user() {
    return this.belongsTo('App/Models/User')
  }


  contract() {
    return this.belongsTo('App/Models/Contract')
  }
}

module.exports = UsersPlan
