'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ReboxPrice extends Model {
  portCar() {
    return this.belongsTo('App/Models/PortCar')
  }
}

module.exports = ReboxPrice
