'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class HistoryRequest extends Model {
  partner() {
    return this.belongsTo('App/Models/Partner')
  }
}

module.exports = HistoryRequest
