'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use('App/Models/ReboxPrice')
class ReboxSurplus extends Model {
  port() {
    return this.belongsTo('App/Models/PortCar')
  }

}

module.exports = ReboxSurplus
