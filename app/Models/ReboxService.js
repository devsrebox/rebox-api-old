'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ReboxService extends Model {

  assessments() {
    return this.hasOne('App/Models/Assessment')
  }

  attendant() {
    return this.belongsTo('App/Models/Attendant')
  }

  partner() {
    return this.belongsTo('App/Models/Partner')
  }

  payments() {
    return this.hasMany('App/Models/StatusPayment')
  }

  vehicle() {
    return this.belongsTo('App/Models/Vehicle')
  }

  reject_partner() {
    return this.belongsToMany('App/Models/Partner').pivotTable('reject_reboxes')
  }

  extracts() {
    return this.hasMany('App/Models/ReboxExtract')
  }

  static get computed() {
    return ['timeDescription']
  }

  getTimeDescription({forecast}) {
    if (!forecast)
      return 'aguardando atendimento'
    const arraySplit = forecast.split(":")

    if (arraySplit[0] == "00") {
      return `${arraySplit[1]} minutos`
    } else {
      if (Number(arraySplit[0]) > 1) {
        if (Number(arraySplit[1]) > 1) {
          return `${Number(arraySplit[0])} horas e ${Number(arraySplit[1])} minutos`
        } else {
          return `${Number(arraySplit[0])} horas e ${Number(arraySplit[1])} minuto`
        }
      } else {
        return `${Number(arraySplit[0])} hora e ${Number(arraySplit[1])} minutos`
      }
    }


  }

  //Get and set
  static get dates() {
    return super.dates.concat(["date"]);
  }
  static castDates(field, value) {
    if (field == "date") return value ? value.format("YYYY-MM-DD") : value;
    else return value ? value.subtract('3', 'hours').format("YYYY-MM-DD HH:mm:ss") : value;
    // else used for created_at / updated_at
  }

}

module.exports = ReboxService
