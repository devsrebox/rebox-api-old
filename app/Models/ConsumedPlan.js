"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class ConsumedPlan extends Model {
  use_plan() {
    return this.belongsTo("App/Models/UsersPlan");
  }
  partner() {
    return this.belongsTo("App/Models/Partner");
  }
}

module.exports = ConsumedPlan;
