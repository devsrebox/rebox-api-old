'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Contract extends Model {
  vehicle() {
    return this.belongsTo('App/Models/Vehicle')
  }

  use_plans() {
    return this.hasMany('App/Models/UsersPlan')
  }

  user() {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = Contract
