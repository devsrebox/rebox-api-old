'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PortCar extends Model {
  prices() {
    return this.hasMany('App/Models/ReboxPrice')
  }
}

module.exports = PortCar
