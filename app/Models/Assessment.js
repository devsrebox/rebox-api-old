'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Assessment extends Model {

    rebox() {
      return this.belongsTo('App/Models/ReboxService')
    }
}

module.exports = Assessment
