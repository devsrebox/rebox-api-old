'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Plan extends Model {
  details() {
    return this.hasMany('App/Models/PlansDetail')
  }
}

module.exports = Plan
