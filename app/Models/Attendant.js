'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Attendant extends Model {
  partner() {
    return this.belongsTo('App/Models/Partner')
  }

  user() {
    return this.belongsTo('App/Models/User')
  }

  rebox() {
    return this.hasMany('App/Models/ReboxService')
  }
}

module.exports = Attendant
