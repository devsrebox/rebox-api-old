'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Partner extends Model {

  types() {
    return this.belongsToMany('App/Models/TypesService').pivotTable('partners_types')
  }

  user() {
    return this.belongsTo('App/Models/User')
  }

  reject_rebox() {
    return this.belongsToMany('App/Models/ReboxService').pivotTable('reject_reboxes')
  }

  requests() {
    return this.hasMany('App/Models/ReboxService')
  }

  bank_account() {
    return this.hasOne('App/Models/BankAccount')
  }

  serviceLocations() {
    return this.hasMany('App/Models/ServiceLocation')
  }

}

module.exports = Partner
