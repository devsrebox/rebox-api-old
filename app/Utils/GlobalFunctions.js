const UsersPlan = use('App/Models/UsersPlan')
const ConsumedPlan = use('App/Models/ConsumedPlan')
const Contract = use('App/Models/Contract')
const consumedPlans = async (users_plan_id, user) => {
  const isValidUserPlan = await UsersPlan.query()
      .where("id", users_plan_id)
      .where("is_active", true)
      .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
      .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
      .where("uses", ">", 0)
      .where("user_id", user.id)
      .first();
    if (!isValidUserPlan)
      return 401

    await ConsumedPlan.create({
      users_plan_id: users_plan_id,
      address: address || "N/A",
      lng,
      lat,
    });

    const contract = await Contract.find(isValidUserPlan.contract_id)
    contract.uses++
    await contract.save()

    isValidUserPlan.uses -= 1;
    if (isValidUserPlan.uses == 0) {
      isValidUserPlan.is_active = false;
    }
    await isValidUserPlan.save();

    const contractIsValid = await UsersPlan.query()
      .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
      .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
      .where("uses", "=", 0)
      .where("user_id", user.id)
      .where('contract_id', contract.id)
      .fetch();

    if (contractIsValid && contractIsValid.rows.length >= MAX_CONSUMED_PLANS) {
      const servicesActives = await UsersPlan.query()
        .where("is_active", true)
        .where("user_id", user.id)
        .where("validity_of", "<=", moment().format("YYYY-MM-DD"))
        .where("validity_until", ">=", moment().format("YYYY-MM-DD"))
        .where('contract_id', contract.id)
        .where("uses", ">", 0)
        .fetch();
      for (const row of servicesActives.rows) {
        row.is_active = false;
        await row.save();
      }
    }
}
