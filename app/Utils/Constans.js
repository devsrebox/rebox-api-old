const Env = use("Env");

module.exports = {
  LOGO_URL: ` ${Env.get("APP_URL")}images/logo.png`,
  MAX_CONSUMED_PLANS: 3,
  LACK_OF_THE_PLAN: 72, //HOURS
  STATUS_PAYMENT: {
    PAGO: "pago",
    PENDENTE: "pendente",
    NAO_AUTORIZADO: "não autorizado",
  },
  STATUS_SERVICE: {
    PENDENTE: "pendente",
    ACEITO: "aceito",
    FINALIZADO: "finalizado",
    ANDAMENTO: "andamento",
  },

  USER_ROLE: {
    USER: "user",
    PARTNER: "partner",
    ADMIN: "admin",
    ATTENDANT: "attendant",
  },
  ARMORED_ID: 7,
  //ID DO DETALHE DO PLANO REBOX E REBOX +
  PLAN_DETAIL_TRAILER_ID: [1, 8],
  //COMISSÃO DA REBOX PARA SERVIÇOS EXCEDENTE
  SURPLUS: 0,

  //PERCENTUAL DA REBOX
  EXTRA: 40,

  REBOXVALUE: function (distanceKM, reboxPrice) {
    var price = reboxPrice.price;
    var surplus = 0;
    if (distanceKM > reboxPrice.km) {
      var extra = distanceKM - reboxPrice.km;
      surplus = extra * reboxPrice.per_km;
      console.log("extra " + extra);
    }
    console.log("distancia em km " + distanceKM);
    return { price, surplus };
  },

  COMMISSIONVALUE: function (value, commission) {
    reboxCommission = (value * commission) / 100;
    partnerCommission = (value * (100 - commission)) / 100;

    return { reboxCommission, partnerCommission };
  },

  REBOXSURPLUS: function (surplus, surplusPrice) {
    var reboxSurplus = (surplus * surplusPrice) / 100;
    var partnerSurplus = (surplus * (100 - surplusPrice)) / 100;

    return { reboxSurplus, partnerSurplus };
  },

  SERVICEEXTRA: function (service) {
    var reboxExtra = (service.price * this.EXTRA) / 100;
    var partnerExtra = (service.price * (100 - this.EXTRA)) / 100;

    return { reboxExtra, partnerExtra };
  },
};
