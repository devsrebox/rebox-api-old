'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

/**
 * Import Auth Routes
 */
require('./auth')
/**
 * Import Auth rebox
 */
require('./rebox')

/**
 * Import Auth users
 */
require('./user')
/**
 * Import Auth users
 */
require('./partner')
/**
 * Import cars
 */
require('./cars')
/**
 * Import contact
 */
require('./contact')
/**
 * Import google
 */
require('./google')
/**
 * Import google
 */
require('./reports')
/**
 * Import admins
 */
require('./admins')
