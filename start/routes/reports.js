'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
Route.group(() => {
  // Route.get('reports/finance/:partnerid', 'ReportsController.finance')
  // Route.get('reports/operational/pdf/:partnerid', 'ReportsController.operationalDetailsToPdf')
  Route.get('reports/operationals/pdf', 'ReportsController.operationalInPdf')
  Route.get('reports/operationals/excel', 'ReportsController.operationalByDateToExcel')
  Route.get('reports/finances/excel', 'ReportsController.financeExecel')
  Route.get('reports/finances/pdf', 'ReportsController.financeInPdf')
  Route.get('reports/extracts/plans', 'ReportsController.viewAmountPlans')

})

.namespace('Reports')
.prefix('v1')

