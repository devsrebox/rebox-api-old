"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");
Route.group(() => {
  Route.post(
    "partners/:partnerid/rebox/:reboxid",
    "ReboxController.confirmStatus"
  );
  Route.post(
    "partners/:partnerid/rebox/:reboxid/status/finish",
    "ReboxController.finishStatus"
  );
  Route.post(
    "partners/:partnerid/rebox/:reboxid/reject",
    "ReboxController.rejectRebox"
  );
})
  .prefix("v1")
  .namespace("Partners/Rebox");
Route.group(() => {
  Route.resource("partners", "PartnersController");
})
  .middleware("auth")
  .namespace("Partners")
  .prefix("v1");

Route.group(() => {
  Route.resource("partners/:partnerid/attendants", "AttendantsController");
})
  .middleware("auth")
  .middleware("partner")
  .namespace("Partners/Attendants")
  .prefix("v1");

Route.group(() => {
  Route.get("partners/accept/:partnerid", "PartnersController.confirmStatus");
  Route.get("partners/block/:partnerid", "PartnersController.changeStatus");
}).namespace("Partners");

Route.group(() => {
  Route.get("reports/finances/excel", "ReportsController.financeExecel");
  Route.get("reports/finances/pdf", "ReportsController.financeInPdf");
  Route.get("reports/operationals/pdf", "ReportsController.operationalInPdf");
  Route.get(
    "reports/operationals/excel",
    "ReportsController.operationalToExecel"
  );
})

  .namespace("Partners/Reports")
  .middleware("partner")
  .middleware("auth")
  .prefix("v1/partners");

Route.group(() => {
  Route.resource(
    "partners/:idpartner/consumed-plans",
    "ConsumedPlansController"
  ).only(["index", "show", "update"]);
})
  .namespace("Partners/ConsumedPlans")
  .middleware("partner")
  .middleware("auth")
  .prefix("v1");
