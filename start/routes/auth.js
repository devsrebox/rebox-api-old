'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
Route.group(() => {
  Route.post('register', 'AuthController.register')
  Route.post('register/partner', 'AuthController.register')

  Route.post('authenticate', 'AuthController.login')
  Route.post('authenticate/partner', 'AuthController.loginPartner')
  Route.post('authenticate/admin', 'AuthController.loginAdmin')
  Route.post('authenticate/attendant', 'AuthController.loginAttendant')

  Route.post('refresh', 'AuthController.refresh')


  Route.post('logout', 'AuthController.logout')
  Route.post('account', 'AuthController.account')
  Route.post('change-password', 'AuthController.changePassword')
  Route.put('update-role', 'AuthController.updateRole')



  Route.post('forgotpassword', 'AuthController.forgotPassword')
  Route.post('validatecode', 'AuthController.validateCode')
  Route.post('resetpassword', 'AuthController.resetPassword')

  // .validator('Clients/ClientRefreshToken')
})
  .prefix('v1/auth')
  .namespace('Auth')
