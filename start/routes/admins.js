/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");
Route.group(() => {
  Route.resource("users", "UsersController");
  Route.post("users/:id/change-password", "UsersController.changePassword");
})
  .middleware("admin")
  .namespace("Admins/Users")
  .middleware("auth")
  .prefix("v1/admins");

Route.group(() => {
  Route.resource("partners", "PartnersController");
  Route.get('partners/state/cities', 'PartnersController.statesAndCities')
  Route.get('partners/state/:state/cities/:city', 'PartnersController.partnersByStatesAndCities')
})
  .middleware("admin")
  .namespace("Admins/Partners")
  .middleware("auth")
  .prefix("v1/admins");

Route.group(() => {
  Route.resource("consumed-plans", "ConsumedPlansController").except([
    "destroy",
  ]);
  Route.get("consumed/partners", "ConsumedPlansController.partners");

})
  .middleware("admin")
  .namespace("Admins/ConsumedPlans")
  .middleware("auth")
  .prefix("v1/admins");

Route.group(() => {
  Route.resource("rebox", "ReboxController").only(["index", "destroy"]);
})
  .middleware("admin")
  .namespace("Admins/Rebox")
  .prefix("v1/admins")
  .middleware("auth");

Route.group(() => {
  Route.post("users/:userid/consumed-plans/:id", "ConsumedPlansController.consumePlan");
  Route.get("users/:userid/consumed-plans", "ConsumedPlansController.index");
})
  .middleware("admin")
  .namespace("Admins/Users/ConsumedPlans")
  .prefix("v1/admins")
  .middleware("auth");

Route.group(() => {
  Route.resource("prices", "PricesController").only(["index", "update"]);
  Route.get("prices/cities", "PricesController.getAllPricesGrouByCities");
})
  .middleware("admin")
  .middleware("auth")
  .namespace("Admins/Prices")
  .prefix("v1/admins");
