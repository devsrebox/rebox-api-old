'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
Route.group(() => {
  Route.resource('rebox', 'ReboxController').except(['destroy'])
  Route.get('rebox/requests/:id', 'ReboxController.requests')
  Route.put('rebox/:id/scheduling-date', 'ReboxController.updateSchedulingDateToRebox')
})
.namespace('Rebox')
.prefix('v1')

Route.group(() => {
  Route.resource('rebox/:reboxid/assessments', 'AssessmentsController').only(['store'])
})
.namespace('Rebox/Assessments')
.prefix('v1')

Route.group(() => {
  Route.post('rebox/:reboxid/payments', 'PaymentsController.store')
})
.namespace('Rebox/Payments')
.prefix('v1')
