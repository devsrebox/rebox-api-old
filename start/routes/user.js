'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
Route.group(() => {
  Route.resource('users/:userid/vehicles', 'VehiclesController').only(['index', 'store'])

})
  .middleware('auth')
  .namespace('Users/Vehicles')
  .prefix('v1')
Route.group(() => {
  Route.resource('users/:id/plans', 'PlansController').only(['index'])
  Route.post('users/:userid/plans/:id/payments', 'PlansController.paymentPlan')
  Route.post('users/plans/:id/consume', 'PlansController.consumePlan')
  Route.get('users/:email/plans/isactive', 'PlansController.isActive')
  Route.get('users/:userid/plans/last', 'PlansController.last')
  Route.get('users/:userid/plans/:id/cancel', 'PlansController.cancel')

})
  .middleware('auth')
  .namespace('Users/Plans')
  .prefix('v1')
Route.group(() => {
  Route.resource('users', 'UsersController').only(['index', 'update', 'show'])
  Route.post('users/email', 'UsersController.getUserById')
  Route.post('users/profile', 'UsersController.updateImgProfile')
})
  .namespace('Users')
  .prefix('v1')
Route.group(() => {
  Route.resource('users/:userid/history-requests', 'HistoryRequestsController').only(['index'])
})
  .namespace('Users/HistoryRequests')
  .prefix('v1')
  .middleware('auth')
